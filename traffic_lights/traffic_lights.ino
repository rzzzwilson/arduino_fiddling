/////////////////////////////////////////////////////////////////////////
// Example code for "state" control of simple traffic lights.
//
// Assumes red, amber and green LEDs on these pins, all from pin to GND:
//     red    13
//     amber  12
//     green  11
// and a switch from pin 10 to ground.
//
// This is the simplest state approach I can think of.  More advanced
// code would have more behaviour defined by data, not code.
//
// This version notices a button press in RED state and switches to the
// GREEN state if RED has been showing long enough.  There is also a blue
// LED ('ready' indication) that turns on when the RED phase may be
// interruoted by the pushbutton.  We do this by adding another state.
/////////////////////////////////////////////////////////////////////////

// pin definitions for the colour LEDs
const int RedPin = 13;      // Iota = C7  Using Leonardo clone called "Iota"
const int AmberPin = 9;     // Iota = B5
const int GreenPin = 11;    // Iota = B7

const int ReadyPin = 12;    // Iota = D6 plus blue LED

// pin definition for the switch
const int SwitchPin = 10;   // Iota = B6

// time for each colour cycle (milliseconds)
// short cycle times used when making a video
const int RedTime = 2500;     // 750;
const int RedIntTime = 2500;  // 750
const int AmberTime = 1000;   // 500;
const int GreenTime = 5000;   // 1500;

// the states, don't change these numbers
#define RED_STATE     0   // red LED on, not interruptible
#define REDINT_STATE  1   // red ON, pushbutton interrupts
#define AMBER_STATE   2
#define GREEN_STATE   3

// state variables
int state = REDINT_STATE; // the current state, green is next
long next_change = 0;     // next change is immediate

void setup()
{
  // define LED pins as output
  pinMode(RedPin, OUTPUT);
  pinMode(AmberPin, OUTPUT);
  pinMode(GreenPin, OUTPUT);
  pinMode(ReadyPin, OUTPUT);

  // the SwitchPin must be INPUT_PULLUP
  pinMode(SwitchPin, INPUT_PULLUP);

  // all LEDs off
  digitalWrite(RedPin, LOW);
  digitalWrite(AmberPin, LOW);
  digitalWrite(GreenPin, LOW);
  digitalWrite(ReadyPin, LOW);
}

void loop()
{
  // check current time
  long now = millis();

  // is the switch closed?  If so, it will read LOW.
  if (digitalRead(SwitchPin) == LOW)
  {
    // switch operated.  If red, switch to green if red showing long enough
    if (state == REDINT_STATE)
    {
        // force an immediate change to the green state which follows red
        next_change = now;
    }
  }
  
  if (now >= next_change)
  {
    // time for a change, depending on current state
    if (state == RED_STATE)
    {
      // was red, move to "red interruptable"
      state = REDINT_STATE;           // new state is "red interruptible"
      next_change = now + RedIntTime; // set time of next change
      digitalWrite(ReadyPin, HIGH);   // turn on bLue LED
    }
    else if (state == REDINT_STATE)
    {
      // was "red interruptable", move to green
      digitalWrite(RedPin, LOW);      // was red, so LED off
      digitalWrite(ReadyPin, LOW);    // "ready" blue LED off
      state = GREEN_STATE;            // new state is green
      next_change = now + GreenTime;  // set time of next change
      digitalWrite(GreenPin, HIGH);   // turn on green LED
    }
    else if (state == GREEN_STATE)
    {
      // was green, move to amber
      digitalWrite(GreenPin, LOW);
      state = AMBER_STATE;
      next_change = now + AmberTime;
      digitalWrite(AmberPin, HIGH);
    }
    else if (state == AMBER_STATE)
    {
      // was amber, move to red
      digitalWrite(AmberPin, LOW);
      state = RED_STATE;
      next_change = now + RedTime;
      digitalWrite(RedPin, HIGH);
    }
  }
}

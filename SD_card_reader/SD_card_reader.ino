#include <SPI.h>
#include <SD.h>

// define as non-zero if using Arduino IDE
#define ARDUINO_IDE  1

// Iota: SCK B1, MOSI B2, MISO B3
#define CSPin 10     // pin B6 on Iota
#define LedPin 13    // builtin LED on M0 Pro

File myFile;

void setup()
{
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  delay(4000);

#if ARDUINO_IDE
  while (!Serial)
    ; // wait for serial port to connect.
#endif

  pinMode(CSPin, OUTPUT);
  digitalWrite(CSPin, LOW);

  pinMode(LedPin, OUTPUT);
  digitalWrite(LedPin, LOW);

  Serial.print("Initializing SD card...");
  if (!SD.begin(CSPin))
  {
    Serial.println("initialization failed!");
    while (1)
      ;
  }
  Serial.println("initialization done.");
  
  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  myFile = SD.open("test.txt", FILE_WRITE);
  
  // if the file opened okay, write to it:
  if (myFile)
  {
    Serial.print("Writing to test.txt...");
    myFile.println("This is a test file :)");
    myFile.println("testing 1, 2, 3.");
    for (int i = 0; i < 20; i++)
    {
      myFile.println(i);
    }
    // close the file:
    myFile.close();
    Serial.println("done.");
  } else
  {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }
}
void loop()
{
  // just blink the LED
  digitalWrite(LedPin, HIGH);
  delay(500);
  digitalWrite(LedPin, LOW);
  delay(500);
}

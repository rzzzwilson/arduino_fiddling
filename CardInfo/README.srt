The code here reads the files on an SD card via SPI using the Iota
and a 5v SD breakout board.  The wiring is:

+------+------+
| Iota |  SD  |
+======+======+
| GND  | GND  |
+------+------+
| Vcc  | Vcc  |
+------+------+
| B6   | CS   |
+------+------+
| B1   | SCLK |
+------+------+
| B2   | SI   |
+------+------+
| B3   | SO   |
+------+------+

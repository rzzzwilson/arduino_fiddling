/*
  Code to exercise the "schedule" module.

  There are three LEDs on pins "led_blue", "led_red" and "led_green".
  Turn them on and off at different times without using arduino delay().
 */

#include "schedule.h"


#define DEBUG
#define TEST_OVERFLOW

// pins controlling three LEDa
const int led_blue = 12;    // Iota D6, blue
const int led_red = 6;      // Iota D7, red
const int led_green = 8;    // Iota B4, green

const int btn = 9;          // Iota B5, pushbutton to ground

// periods for each LED
const int blue_period = 97;     // three primes,
const int red_period = 487;     //  almost multiples of the previous
const int green_period = 1459;

//======================================================================
// Code to exercise the "schedule" module.
//
// There are three LEDs on pins "led_blue", "led_red" and "led_green".
// Turn them on and off at different times.
//======================================================================

bool abort_flag = false;    // true if the code should abort

void event_abort(void)
{
  delay(3000);      // in case Serial not working yet (AtMEGA32U4, etc)
  Serial.println("ABORT: event_abort() called!");
  abort_flag = true;
}

void blue_on(void)
{
  digitalWrite(led_blue, HIGH);
  sched_sched(blue_period, blue_off);
}

void blue_off(void)
{
  digitalWrite(led_blue, LOW);
  sched_sched(blue_period, blue_on);
}

void red_on(void)
{
  digitalWrite(led_red, HIGH);
  sched_sched(red_period, red_off);
}

void red_off(void)
{
  digitalWrite(led_red, LOW);
  sched_sched(red_period, red_on);
}

void green_on(void)
{
  digitalWrite(led_green, HIGH);
  sched_sched(green_period, green_off);
}

void green_off(void)
{
  digitalWrite(led_green, LOW);
  sched_sched(green_period, green_on);
}

void start_led(void)
{
  // exercise all LEDs as a signon test
  digitalWrite(led_blue, HIGH);
  digitalWrite(led_red, HIGH);
  digitalWrite(led_green, HIGH);
  delay(1000);
  digitalWrite(led_blue, LOW);
  digitalWrite(led_red, LOW);
  digitalWrite(led_green, LOW);
  delay(500);

  // start all LED colour cycles at same time after a delay
  sched_sched(red_period, red_on);
  sched_sched(green_period, green_on);
  sched_sched(blue_period, blue_on);
}

extern volatile unsigned long timer0_millis;

void set_overflow(void)
{
  // set millis() overflow for 5 seconds from now
  noInterrupts();
  timer0_millis = (unsigned long) -5000;
  interrupts();
  Serial.println("Overflow in 5 seconds!");
}

void setup(void)
{
  // initialize the Serial connection, if required
  Serial.begin(115200);
#ifdef DEBUG
  delay(3000);      // required for AtMega32U4 Serial system
#endif

  // initialize the LED pins as OUTPUT
  pinMode(led_blue, OUTPUT);
  pinMode(led_red, OUTPUT);
  pinMode(led_green, OUTPUT);
  pinMode(btn, INPUT_PULLUP);

#ifdef TEST_OVERFLOW
  set_overflow();
#endif

  // prepare the schedule system, 5 entries, only need 3
  sched_init(event_abort, 5);

  // start the LEDs
  start_led();
}

void loop(void)
{
  if (abort_flag) // if we are aborted, do nothing
    return;       // have to keep loop()ing on an Arduino

#ifdef TEST_OVERFLOW
  // print if we overflow on the millis() value
  static unsigned long last_millis = 0;
  if (millis() < last_millis)
  {
    Serial.println("OVERFLOW!");
  }
  last_millis = millis();
#endif

  if (digitalRead(btn) == LOW)
  {
    Serial.println("Button pushed!");
    sched_flush();
    setup();      // simple, just call setup() again
  }

  // show we aren't waiting on delay() anywhere
  // this shows we get 4 to 6 loop() calls per millisecond
  //Serial.println(millis());
  
  sched_tick();   // kick the schedule system along
}

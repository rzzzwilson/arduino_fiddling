/*
 * Schedule
 *
 * Code to handle asynchronous "events" in code without using delay().
 *
 * An attempt has been made to make the system impervious to the millis()
 * rollover every 49+ days.  This should be considered to be untested.
 */

#include <Arduino.h>    // needed for Serial, etc
#include "schedule.h"

// max allowed value for the scheduled delay (half max overflow period)
#define MAX_WAIT  0x8FFFFFFF

//#define SCHED_DEBUG

// definition of an Event struct
typedef struct { unsigned long start;     // time event scheduled
                 unsigned long period;    // period of wait
                 event_handler handler;   // function executed when event triggers
               } Event;

// stuff to handle the Event array
// The array is sorted such that the earlies Event is in slot 0,
// the next Event is at 1 and so on.
static int NumEvents = 0;           // number of Event slots in the system
static int NextSlot = 0;            // index of first free Event slot in "EventArray" array
static Event *EventArray = NULL;    // array of Event structs

// address of the user function to call on abort, if given.
static event_handler SchedAbortHandler = NULL;  // address of user "abort" function


//***************************************************************************
// Debug routine - dump contents of EventArray to Serial.
//     msg  message to print as part of the dump
//***************************************************************************

#ifdef SCHED_DEBUG
static void sched_dump(const char *msg)
{
  Serial.println("EVENTS  ---------------------------------------");
  Serial.print(msg);
  Serial.print("| NextSlot=");
  Serial.println(NextSlot);
  Serial.println("-----------------------------------------------");
  
  for (int i=0; i < NextSlot; ++i)
  {
    Serial.print("i: ");
    Serial.print(i);
    Serial.print(", .start=");
    Serial.print(EventArray[i].start, HEX);
    Serial.print(", .period=");
    Serial.print(EventArray[i].period, HEX);
    Serial.print(", handler=");
    Serial.println((int) EventArray[i].handler, HEX);
  }
  
  Serial.println("===============================================");
}
#endif

//***************************************************************************
// Routine to initialize the "schedule" system.
//     abort_handler  address of handler function on abort
//     num_events     required slots in the EventArray array
// If "abort_handler" is NULL the handler will not be called on abort.
//***************************************************************************

void sched_init(event_handler abort_handler, int num_events)
{
  NextSlot = 0;
  SchedAbortHandler = abort_handler;
  NumEvents = num_events;
  
  if (EventArray != NULL)     // if called twice free previously allocated memory
  {
    free(EventArray);
  }
  EventArray = (Event *) malloc(sizeof(Event) * NumEvents);
}

//***************************************************************************
// Routine to flush events from the "schedule" system.
//
// All events are removed from the system, abort handler and table size
// remain unchanged.
//***************************************************************************

void sched_flush(void)
{
  NextSlot = 0;
}

//***************************************************************************
// Routine to schedule an Event in the system.
//     wait     time in ms to wait from now before event is triggered
//     handler  address of handler function to call when event is triggered
//***************************************************************************

void sched_sched(unsigned long wait, event_handler handler)
{
#ifdef SCHED_DEBUG
  Serial.print("sched_sched: wait=");
  Serial.print(wait);
  Serial.print(", handler=");
  Serial.println((int) handler, HEX);
#endif

  if (wait > MAX_WAIT)
  {
    Serial.print("sched_sched: ERROR! wait=");
    Serial.print(wait, HEX);
    Serial.print(", maximum allowed=");
    Serial.println(MAX_WAIT, HEX);
    return;
  }

  // check if there are any free slots, if not call abort handler, if defined
  if (NextSlot >= NumEvents)
  {
    if (SchedAbortHandler != NULL)
      SchedAbortHandler();
    return;
  }

  // just put event into next free slot
  EventArray[NextSlot].start = millis();
  EventArray[NextSlot].period = wait;
  EventArray[NextSlot].handler = handler;

  ++NextSlot;
}

//***************************************************************************
// The "tick" function.  Should be called periodically in user code to check
// if an event needs to be triggered.
//***************************************************************************

void sched_tick(void)
{
#ifdef SCHED_DEBUG
  Serial.print("now=");
  Serial.println(now, HEX);
  sched_dump("sched_tick: before");
#endif

  // scan through the slots
  int slot = 0;                     // slot scan index
  
  while (slot < NextSlot)
  {
    // see if slot is to be executed
    if (millis() - EventArray[slot].start > EventArray[slot].period)
    {
      // going to execute this slot, save handler address
      event_handler handler = EventArray[slot].handler;

      // remove event at this slot, move others down
      memmove(&EventArray[slot], &EventArray[slot+1], sizeof(Event) * (NextSlot - slot - 1));
      --NextSlot;       // decrease slot count

      // execute handler
      handler();        // could schedule new task, changing "next_slot"
    }
    else
    {
      // slot not executed, bump "slot" to search next slot
      ++slot;
    }
  }

#ifdef SCHED_DEBUG
  sched_dump("sched_tick: after");
#endif
}

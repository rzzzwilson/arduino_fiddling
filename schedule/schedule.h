/*
 * Schedule interface
 *
 * Code to handle asynchronous "events" in code without using delay().
 */

#ifndef __SCHEDULE_H__
#define __SCHEDULE_H__


// typedef for user function to handle an event
typedef void (*event_handler)(void);

// functions advertised by schedule.cpp
void sched_init(event_handler abort_handler, int num_events);
void sched_flush(void);
void sched_sched(unsigned long period, event_handler handler);
void sched_tick(void);


#endif

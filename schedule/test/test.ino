/*
 * Code to test handling of times in "schedule" code.
 *
 * Create an array of test events and check handling when
 * adding new event to array.
 */

#include "schedule.h"


#define MAX_ULONG  0xFFFFFFFF

void sched(unsigned long period, event_handler handler);


Event Events[10];
int next_slot = 0;


void test1(void)
{
  Serial.print("test1: millis()=");
  Serial.println(millis());
  sched(30000, test1);
}

void test2(void)
{
  Serial.print("test2: millis()=");
  Serial.println(millis());
  sched(20000, test2);
}

void test3(void)
{
  Serial.print("test3: millis()=");
  Serial.println(millis());
  sched(10000, test3);
}

void test4(void)
{
  Serial.print("test4: millis()=");
  Serial.println(millis());
  sched(MAX_ULONG - 300, test4);
}

void test5(void)
{
  Serial.print("test5: millis()=");
  Serial.println(millis());
  sched(MAX_ULONG - 1200, test5);
}

void test6(void)
{
  Serial.print("test6: millis()=");
  Serial.println(millis());
  sched(500, test6);
}


const char *decode_handler(event_handler handler)
{
  if (handler == test1) return "test1";
  if (handler == test2) return "test2";
  if (handler == test3) return "test3";
  if (handler == test4) return "test4";
  if (handler == test5) return "test5";
  if (handler == test6) return "test6";

  return "unknown";
}


void dump_events(const char *msg)
{
  Serial.println("");
  Serial.print(msg);
  Serial.print("   now=");
  Serial.println(millis());
  Serial.println("----------------------------------------------");
  
  for (int i = 0; i < next_slot; ++i)
  {
    Event *ptr = &Events[i];

    Serial.print(i);
    Serial.print(": start=");
    Serial.print(ptr->start);
    Serial.print(", period=");
    Serial.print(ptr->period);
    Serial.print(", handler=");
    Serial.println(decode_handler(ptr->handler));
  }

  Serial.println("----------------------------------------------");
}


//***************************************************************************
// See if any event can be executed.
//***************************************************************************

void tick(void)
{
  int slot = 0;   // slot number we are looking at
  
  while (slot < next_slot)
  {
    unsigned long now = millis();
    
    // see if slot is to be executed
    if (now - Events[slot].start > Events[slot].period)
    {
      // going to execute this slot, save handler address
      event_handler handler = Events[slot].handler;

      // remove event at this slot, move others down
      memmove(&Events[slot], &Events[slot+1], sizeof(Event) * (next_slot - slot - 1));
      --next_slot;      // decrease "next_slot"

      // execute handler
      handler();        // could schedule new task, changing "next_slot"
    }
    else
    {
      // slot not executed, bump "slot" to search next slot
      ++slot;
    }
  }
}


//***************************************************************************
// Schedule an event.
//     period  the time from now (msec) to execute the event
//     event   routine to call when event executes
//
// Just store event in the next slot.
//***************************************************************************

void sched(unsigned long period, event_handler handler)
{
  unsigned long now = millis();

  Events[next_slot].start = now;
  Events[next_slot].period = period;
  Events[next_slot].handler = handler;

  ++next_slot;
}


void setup()
{
  Serial.begin(115200);
  delay(1000);

  sched(30000, test1);
  sched(20000, test2);
  sched(10000, test3);
  sched(MAX_ULONG - 300, test4);
  sched(MAX_ULONG - 1200, test5);

//  sched(100, test6);
//   sched(300, test6);
//    sched(MAX_ULONG - 900, test6);
//  sched(MAX_ULONG - 300, test6);

  while (true)
    if (Serial)
      break;
}

void loop()
{
  tick();
}

/*
 * Schedule interface
 *
 * Library to handle asynchronous "events" in code without using delay().
 */

#ifndef __SCHEDULE_H__
#define __SCHEDULE_H__

#include "Arduino.h"

// if defined, include DEBUG code
//#define SCHED_DEBUG

// typedef for user function to handle an event
typedef void (*event_handler)(void);

// definition of an Event struct
typedef struct { unsigned long start;     // time event scheduled
                 unsigned long period;    // time after "start" when triggered
                 event_handler handler;   // function executed when event triggers
               } Event;

class Schedule
{
  public:
    Schedule(event_handler abort_handler, int num_events);
    ~Schedule();
    void flush(void);
    void schedule(unsigned long delay, event_handler handler);
    void tick(void);
  private:
    Event *EventArray;                  // array of "Event"s
    int NextSlot;                       // index of first free Event slot in "EventArray"
    int NumEvents;                      // number of Event slots in the EventArray array
    
    event_handler AbortHandler = NULL;  // address of user "abort" function
    
    void dump(const char *msg);         // debug dump to Serial
    void remove(unsigned int slot);     // remove slot "slot" in EventArray array
};

#endif

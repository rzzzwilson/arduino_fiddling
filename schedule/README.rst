schedule
========

This is a small library to make the handling of asynchronous events easier
without using *delay()*.  An attempt to handle the *millis()* rollover has
been made, but should be considered experimental.

Example Program
---------------

A small piece of arduino code::

    #include "schedule.h"

    void do_print(void) {
      Serial.print("Tick: ");
      Serial.println(millis());
      sched_sched(1500, do_print);
    }

    void setup() {
      Serial.begin(115200);
      delay(3000);        // AtMEGA32U4, so wait
      sched_init(NULL, 2);
      sched_sched(1500, do_print);
    }

    void loop() {
      sched_tick();
    }

When executed on an Arduino we see this in the Serial Monitor::

    Tick: 4500
    Tick: 6000
    Tick: 7500
    Tick: 9000
    Tick: 10500
    Tick: 12000
    Tick: 13500
    Tick: 15000
    Tick: 16500
    ...


Look at *schedule.ino* for a more ambitious example that flashes three LEDs.

API
---

**void sched_init(event_handler abort_handler, int num_events);**

The *sched_init()* function must be called first to initialize the system.

The *abort_handler* parameter is the address of a user function taking no
parameters and returning nothing that does whatever needs to be done on a
**schedule** abort.  The abort condition occurs when user code tries to schedule
too many events for the internal table.  If the *abort_handler* is NULL then no
action is taken.

The **num_events** parameter is the number of event slots created in an internal
table.  If this table fills up an abort occurs.

--------

**void sched_sched(unsigned long period, event_handler handler);**

The *sched_sched()* function is used by user code to schedule an event for the
future.

The *period* parameter is the number of milliseconds in the future the event is
triggered.

The *handler* parameter is the address of a user function that takes no
parameters and returns nothing.  This function is called when the event is
triggered.

Note that when an event is triggered it is removed from the *sched* table.  If
you want a function to be called repeatedly you must make the function code
reschedule itself before terminating.  See the example code.

---------

**void sched_tick(void);**

The *sched_tick()* function should be called regularly by user code.  The event
table is scanned and pending events that are past their trigger time are
executed.

millis() rollover
-----------------

The unsigned long integer returned by the *millis()* function increases for more
than 49 days before "rolling over" to 0 and starting to increase again.

An attempt has been made to make the *schedule* code handle any problems the
rollover might produce, but this has not been fully tested.

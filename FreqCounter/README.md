# Frequency Counter

This is a simple frequency counter using an Arduino Uno or similar.
The design is from "Arduino Projects for Amateur Radio", by Purdum and Kidder.

The circuit will measure up to a little more than 6MHz "bare" on the Uno
(16MHz clock), and has provision for adding a pre-scaler that divides by 2 or
4 times.  This can increase the measured frequency up to 24MHz or so on the Uno.

Think about using this basic idea with:

* 20MHz clock
* Auto scaling

## 20MHz clock

Running the AtMega at 20MHz will increase the upper limit of the frequency it
can measure, I think.  To be tested.

## Auto Scaling

Using some sort of switch to control which of the three outputs from the
pre-scaler is used will automate usage.

Whenever switched on (or a frequency change is noticed, or some other method)
the device will automatically use the x4 pre-scaler output initially.  Once a
frequency is measured the software may decide to switch to the x2 or x1
pre-scaler output.

## Documentation

The conditioning input schematic:

![The conditioning input](InputConditioner.png "Input conditioner")

and prescaler schematic:

![The prescaler](Prescaler.png "pescaler")

## Other Directories

This directory contains everything need to program the Uno.  Other directories
contain original code or other code that might be used later.

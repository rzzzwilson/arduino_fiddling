#include <LiquidCrystal.h>
#include "FreqCounter.h"


#define DEFAULTPRECISION    6       // default display decimal places
#define VALUEWIDTH          10      // numeric field width

#define LCDNUMCOLS          16      // # columns on LCD
#define LCDNUMROWS          2       // # rows on LCD

#define STARTUPDELAY        3000    // splash screen delay
#define COUNTDELAY          20      // wait between measurements

#define CALIBRATION         0       // arduino clock adjustment

#define GATETIME            100     // gate time in milliseconds

#define MEGAHERTZ           1000000 // # cycles in 1 MHz
#define MILLISECONDS        1000    // # msec in one second

// prescaler constants, uncomment the applicable prescale value
#define PRESCALER           1
//#define PRESCALER           2
//#define PRESCALER           4

//#define FIRSTIF             12.0        // IF frequency of radio
//#define CALOFFSET           -0.004988   // determined by testing...
#define FIRSTIF             0.0         // IF frequency of radio
#define CALOFFSET           -0.001345   // determined by testing...


char scale[] = " MHz";                  // scale string for display units
float scaleFactor = 1.0;                // scale factor to adjust units

// pins for the 1602 display
#define RS  12
#define EN  11
#define RW  10
#define D4  9
#define D5  8
#define D6  7
#define D7  6

LiquidCrystal lcd(RS, RW, EN, D4, D5, D6, D7); // setup the 1602 display

void setup(void)
{
    lcd.begin(LCDNUMCOLS, LCDNUMROWS);

    // splash screen
    lcd.print("Freq Counter ");
    lcd.setCursor(0, 1);
    lcd.print("ver 1.0 27Mar14");
    delay(STARTUPDELAY);

    // prepare for measurements
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Frequency:");
}

void loop(void)
{
    char buffer[LCDNUMCOLS + 1];
    unsigned long dataIn;
    float val;

    FreqCounter::f_comp = CALIBRATION;  // set calibration value
    FreqCounter::start(GATETIME);       // count pulses for gate time

    while (FreqCounter::f_ready == 0)
    {
        dataIn = FreqCounter::f_freq;
    }
    
//    dataIn = FreqCounter::f_freq;

    delay(COUNTDELAY);

    val = (float) dataIn * PRESCALER * scaleFactor * (MILLISECONDS / GATETIME) / MEGAHERTZ;
    val += CALOFFSET;

    dtostrf(val, VALUEWIDTH, DEFAULTPRECISION, buffer); // convert to string
    strcat(buffer, scale);

    lcd.setCursor(0, 1);
    lcd.print(buffer);
}

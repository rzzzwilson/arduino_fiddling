/*
 * Test code for the StateButton library.
 * 
 * Attach button from pin 5 to ground.  Observe serial
 * monitor while opening/closing switch.
 */

#include "StateButton.h"

// define the pin the button is connected to
const byte button_pin = 5;

// define the StateButton
StateButton test_button = StateButton(button_pin);

// previous printed state
// use this to only print state changes
button_state last_print_state = BUTTON_UNKNOWN;

void setup()
{
    Serial.begin(115200);

    // set "last printed value" to current value
    last_print_state = test_button.poll();

    // on Atmega32U4 chips we need to wait
    while (true)
    {
        if (Serial)
            break;
        delay(100);
    }
    Serial.println("OK");
}

void loop()
{
    // current state of the button
    button_state curr_state = test_button.poll();
  
    if (last_print_state != curr_state)
    {
        // only print if state has changed
        Serial.print("button: ");
        Serial.print(millis());
        Serial.print("  ");
        Serial.println(test_button.decode(curr_state));
    }

    // update last printed state
    last_print_state = curr_state;
}

#ifndef STATEBUTTON_H
#define STATEBUTTON_H

#include <Arduino.h>

// possible button states
typedef enum
{
    BUTTON_UNKNOWN,
    BUTTON_LOW,
    BUTTON_RISE,
    BUTTON_HIGH,
    BUTTON_FALL
} button_state;

class StateButton
{
  public:
    StateButton(byte);
    button_state poll(void);
    char *decode(button_state);

  private:
    // translate state to string
    unsigned long past_reads;   // last 32 reads of the pin
    unsigned char prev_state;   // previous state
    unsigned char pin;          // the pin # the button is attached to
};

#endif

/*
 * Library to poll and debounce multiple buttons connected
 * from pin to ground.  Usage:
 *
 *      #include <StateButton>
 *      const byte button_pin = 5;
 *      StateButton test_button = StateButton(button_pin);
 *
 *      void loop(void)
 *      {
 *          button_state = test_button.poll();
 *
 * There is small delay at the end og the poll() method.
 * If the loop() code does more than just poll buttons you
 * can try removing the small delay.  Using a single very
 * noisy "test" button I needed the delay.
 */

#include "StateButton.h"


/////////////////////////////////////////////////////////////
// Constructor that handles the creation of StateButton.
//     pin  the physical pin the button is connected to
/////////////////////////////////////////////////////////////

StateButton::StateButton(byte pin_number)
{
    // save the pin number
    pin = pin_number;

    // make the pin an INPUT with PULLUP
    pinMode(pin_number, INPUT_PULLUP);

    // set other state valiables
    prev_state = digitalRead(pin_number);
    if (prev_state)
        past_reads = 0xffffffff;
    else
        past_reads = 0x00000000;
}


/////////////////////////////////////////////////////////////
// Function to read the "state" of the button.
//
// Implements switch debounce and returns one of:
//     BUTTON_LOW    state was LOW, now LOW
//     BUTTON_RISE   state was LOW, now HIGH
//     BUTTON_HIGH   state was HIGH, now HIGH
//     BUTTON_FALL   state was HIGH, now LOW
/////////////////////////////////////////////////////////////

button_state StateButton::poll(void)
{
    byte curr_state = digitalRead(pin);
    
    // add current state to the past reads buffer
    past_reads = (past_reads << 1) | curr_state;

    if (past_reads == 0x00000000)
        curr_state = LOW;
    else if (past_reads == 0xffffffff)
        curr_state = HIGH;
  
    // quick compare of previous and current states
    button_state result = BUTTON_UNKNOWN;
  
    switch ((prev_state << 8) + curr_state)
    {
        case ((LOW << 8) + LOW):   result = BUTTON_LOW;  break;
        case ((LOW << 8) + HIGH):  result = BUTTON_RISE; break;
        case ((HIGH << 8) + HIGH): result = BUTTON_HIGH; break;
        case ((HIGH << 8) + LOW):  result = BUTTON_FALL; break;
    }
  
    // we must remember the (new) previous state
    prev_state = curr_state;

    // if you have almost nothing in the loop() function you might get 
    // button noise unless the small delay below is used
    delayMicroseconds(500);

    return result;
}

/////////////////////////////////////////////////////////////
// Private method to convert a "button_state" to a string..
//     state  the button state enum to convert
/////////////////////////////////////////////////////////////

char * StateButton::decode(button_state state)
{
    switch (state)
    {
        case BUTTON_UNKNOWN: return (char*) "BUTTON_UNKNOWN";
        case BUTTON_LOW:     return (char*) "BUTTON_LOW";
        case BUTTON_RISE:    return (char*) "BUTTON_RISE";
        case BUTTON_HIGH:    return (char*) "BUTTON_HIGH";
        case BUTTON_FALL:    return (char*) "BUTTON_FALL";
    }
    return (char*) "?UNKNOWN?";
}

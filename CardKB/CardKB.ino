/*  
 *  Code to test operation of the M5Stack CardKB.
 *  
 *  I2C connections:
 *
 *  |CardKB    | Uno    |
 *  |----------|--------|
 *  |Black     | GND    |
 *  |Red       | 5V     |
 *  |Yellow    | SDA    |
 *  |White     | SCL    |
 */
#include <Wire.h>

#define CARDKB_ADDR 0x5F

void setup()
{
  Wire.begin(); // Initiate the Wire library
  Serial.begin(115200);
//  delay(100);

  pinMode(5, INPUT);
  digitalWrite(5, HIGH);

  Wire.begin();
}

void loop()
{
  Wire.requestFrom(CARDKB_ADDR, 1);
  while(Wire.available())
  {
    char c = Wire.read(); // receive a byte as character
    if (c != 0)
    {
      if (c >= ' ' && c <= '~')
      {
        Serial.print("0x");
        Serial.print(c, HEX);
        Serial.print(" '");
        Serial.print(c);
        Serial.println("'");
      }
      else
      {
        Serial.print("0x");
        if (c < 0x10)
          Serial.print("0");
        Serial.println(c, HEX);
      }
    }
  }
}

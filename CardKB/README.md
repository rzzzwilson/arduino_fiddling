Source code from:

https://github.com/m5stack/M5Stack/tree/master/examples/Unit/CardKB

Product page:

https://docs.m5stack.com/#/en/unit/cardkb

Connections
-----------

|CardKB    | Uno    |
|----------|--------|
|Black     | GND    |
|Red       | 5V     |
|Yellow    | SDA    |
|White     | SCL    |

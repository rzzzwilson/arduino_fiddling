This is a simple little device that uses the DFPlayer MP3 player board.  The
`tutorial followed is here <https://circuitjournal.com/how-to-use-the-dfplayer-mini-mp3-module-with-an-arduino>`_.

The circuit showing the layout used with a Uno R3 is:

.. image:: Circuit_small.png

There is a "fire" SPST switch wired from pin 7 to ground.  This is used to
control the sound.  If the fire button is down while power is applied the
code moves to the next file on the microSD card.

The firmware used to play sounds is in the file *Vickers.ino*.  The aim is to
have a sound file that starts playing when the "fire" button is pressed and the
sound is stopped when the button is released.  The sound files names must start
with a 4 digit number, followed by any text.  Something like
*0001_VickersMG.mp3*.

The sound files are:

+---------------------------+-----------------------------------------------------+
| 0001_VickersMG.mp3        | 14 second burst of a Vickers machine gun            |
+---------------------------+-----------------------------------------------------+
| 0002_InertiaStart_AN2.mp3 | an An-2 Colt engine start using the inertia starter |
+---------------------------+-----------------------------------------------------+
| 0003_YoureDespicable.mp3  | Daffy Duck                                          |
+---------------------------+-----------------------------------------------------+
| 0004_ShootHimNow.mp3      | Daffy Duck. The noise an eager GIB might make.      |
+---------------------------+-----------------------------------------------------+

The sound files are in the *sounds* subdirectory.

Rationale
---------

The changes made to the tutorial circuit (adding the "fire" switch) were made
because I wanted to make a device that could be built into a WW1 replica
aircraft that would produce the sound of a machine gun.  The switch had to
behave like a fighter gun button.  Pressing the button fired the gun until the
button is released.

The DFPlayer board is a little limited.  I initially tried putting a single
round gunshot sound file onto the SD card and repeatedly play the sound while
the button was pressed.  But this didn't work because the player board was slow
repeat the sound file.  The Vickers has a cyclic rate of 450 to 500 per minute
and the repeated file was way slower than that.

Then I tried taking a longer burst file and use Audacity to cut and paste
sections of that file into an approximately 14 second burst.  Now pressing the
button starts playing the file and releasing the button instructs the player to
stop playing.  This was adequate.  There is a maximum limit of 14 second bursts,
but this is a very long burst in air fighting, so this should be OK.

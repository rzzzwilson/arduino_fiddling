////////////////////////////////////////////////////////////////////////////
// Firmware to control the DFPlayer module to play a sound file when a
// button is pushed.
//
// If the "fire" button is down when the device boots it cycles to the
// next file on the microSD card.  The new sound file plays if the
// button is held down.
////////////////////////////////////////////////////////////////////////////

#include <EEPROM.h>
#include <ezButton.h>
#include <SoftwareSerial.h>
#include <DFRobotDFPlayerMini.h>


//-----------------------------------------------------
// force an EEPROM reset if defined non-zero
// DON'T FORGET TO SET BACK TO ZERO AFTER BOOTING!
//
// You shouldn't need this as the EEPROM CRC check
// initializes EEPROM for new microcontrollers, etc.
#define EEPROM_RESET        0
//-----------------------------------------------------


// firmware version
#define VERSION             "2.5"

// do debug prints to Serial if non-zero
#define DEBUG               0

// pin for the "response" LED
// comment the line out if no response LED used
#define RESPONSE_PIN        LED_BUILTIN

// the pin for the "fire" and "config" buttons
#define FIRE_PIN            7

// pins used to communicate with DFPlayer Mini
#define MP3_TX_PIN          2   // through 5->3 level shift
#define MP3_RX_PIN          3

// definitions for the player state
#define STATE_STOPPED       0
#define STATE_PLAYING       1

// set the default volume, maximum volume
#define DEFAULT_VOLUME      20
#define MAX_VOLUME          25

// default filenumber & max number of MP3 files on the SD card
#define DEFAULT_FILE_NUMBER 1
#define MAX_FILE_NUMBER     4

// panic codes
// the RESPONSE_PIN LED will flash this number of times
#define PANIC_PLAYER_FAILURE  3

// struct defining EEPROM stored data
typedef struct
{
  uint8_t FileNumber;       // number of file to play (1->MAX_FILE_NUMBER)
  uint8_t Volume;           // volume to play file (1->MAX_VOLUME)  
} EepromData;

//////////
// Start of variables
//////////

// variables restored from and saved to EEPROM
EepromData Data;
unsigned long Checksum = 0;                 // place to save EEPROM data CRC
#define EEPROM_DATA_SIZE    (sizeof(Data))

// define SoftwareSerial interface to the player
SoftwareSerial softwareSerial(MP3_RX_PIN, MP3_TX_PIN);

// the button to FIRE with
ezButton button(FIRE_PIN);

// Create the DFPlayer object
DFRobotDFPlayerMini player;

// device state, default not playing
uint8_t PlayerState = STATE_STOPPED;

//////////
// Define EEPROM layout for remembered state
//////////

// define start addresses in EEPROM of the variables and checksum
#define EEPROM_DATA         (0)
#define EEPROM_CHECKSUM     (EEPROM_DATA + EEPROM_DATA_SIZE)


//##############################################################################
// Utility routines.
//##############################################################################

//--------------------------------------------------------
// Provide a "printf()"-like function, shortens code.
//     msg  format string
//     ...  varargs stuff
//
// Only prints if DEBUG is defined.
// Problems will occur if the full message exceeds "tmp_buff" size.
//--------------------------------------------------------

void debugf(const char *fmt, ...)
{
#ifdef DEBUG
  va_list ptr;
  char tmp_buff[128];

  memset(tmp_buff, 0, sizeof(tmp_buff));

  va_start(ptr, fmt);
  vsprintf(tmp_buff, fmt, ptr);
  if (strlen(tmp_buff) > sizeof(tmp_buff))
  {
    Serial.println(F("Buffer overflow in debugf()!?"));
  }
  va_end(ptr);

  Serial.print(tmp_buff);
#endif
}

//----------------------------------------
// toggle an output pin.
//
//     num - number of times to flash.
//
// Doesn't matter if already on or off, still toggles.
//----------------------------------------

void togglePin(int pin)
{
  digitalWrite(pin, !digitalRead(pin));
}

//----------------------------------------
// flash the response LED.
//
//     num - number of times to flash.
//----------------------------------------

void flash(uint8_t num)
{
#ifdef RESPONSE_PIN
  for (int i=0; i < num; ++i)
  {
    togglePin(RESPONSE_PIN);
    delay(200);
    togglePin(RESPONSE_PIN);
    delay(200);
  }
#endif
}

//----------------------------------------
// "Panic" routine.  Flashes led a number of times, repeats.
//
//     code - number of time to flash LED
//
// Doesn't return, we hang here.
//----------------------------------------

void panic(int code)
{
  while (1)
  {
    flash(code);
    delay(1500);
  }
}

//----------------------------------------
// Calculate the CRC of the saved data in EEPROM
//----------------------------------------

unsigned long eeprom_crc(void)
{
  const unsigned long crc_table[16] = {0x00000000, 0x1db71064, 0x3b6e20c8, 0x26d930ac,
                                       0x76dc4190, 0x6b6b51f4, 0x4db26158, 0x5005713c,
                                       0xedb88320, 0xf00f9344, 0xd6d6a3e8, 0xcb61b38c,
                                       0x9b64c2b0, 0x86d3d2d4, 0xa00ae278, 0xbdbdf21c
                                      };
  unsigned long crc = ~0L;

  for (unsigned int index = EEPROM_DATA; index < EEPROM_DATA_SIZE; ++index)
  {
    crc = crc_table[(crc ^ EEPROM[index]) & 0x0f] ^ (crc >> 4);
    crc = crc_table[(crc ^ (EEPROM[index] >> 4)) & 0x0f] ^ (crc >> 4);
    crc = ~crc;
  }

  return crc;
}

//----------------------------------------
// Saves the in-memory data to EEPROM and refreshes the checksum.
//----------------------------------------

void save_eeprom_data(void)
{
  // save in-memory data to EEPROM
  EEPROM.put(EEPROM_DATA, Data);
  
  // calculate CRC of the data
  Checksum = eeprom_crc();
  EEPROM.put(EEPROM_CHECKSUM, Checksum);
}

//----------------------------------------
// Gets the data in EEPROM and refreshes the RAM copies.
//
// If the EEPROM checksum is invalid, initializes EEPROM and sets
// in-memory copy to initialized values.
//----------------------------------------

void restore_eeprom_data(void)
{
  // get data checksun
  EEPROM.get(EEPROM_CHECKSUM, Checksum);

  // get actual CRC of data and ensure same, if not, initialize
  if (Checksum != eeprom_crc() || EEPROM_RESET > 0)
  {
    debugf("Resetting EEPROM\n");
    
    // initialize data to default values
    Data.FileNumber = DEFAULT_FILE_NUMBER;
    Data.Volume = DEFAULT_VOLUME;

    // save initialized data
    save_eeprom_data();
  }

  // get data stored in EEPROM
  EEPROM.get(EEPROM_CHECKSUM, Checksum);
  EEPROM.get(EEPROM_DATA, Data);
}


//*********************************************************
// Set up hardware and firmware.
//*********************************************************

void setup()
{
  // configure state, either true or false
  bool configuring = false;

  // configure the fire button pin and outputs
  pinMode(FIRE_PIN, INPUT_PULLUP);
  if (digitalRead(FIRE_PIN) == LOW)
  {
    configuring = true;
  }

#ifdef RESPONSE_PIN
  // used for "problem" indication and feedback
  pinMode(RESPONSE_PIN, OUTPUT);
#endif
  
  // Init USB serial port for debugging
#if DEBUG > 0
  Serial.begin(115200);
  delay(2500);
#endif

  // get volume/filenumber data from EEPROM
  restore_eeprom_data();

  // debounce time for the button
  button.setDebounceTime(5);

  // Init serial port for DFPlayer Mini
  softwareSerial.begin(9600);

  // Start communication with DFPlayer Mini
  if (!player.begin(softwareSerial))
  {
#if DEBUG == 0
    Serial.begin(115200);   // turn on Serial if not already on
    delay(2500);
#endif

    Serial.println("Connecting to DFPlayer Mini failed!");
    Serial.println("Check connections, insert SD card.");
    panic(PANIC_PLAYER_FAILURE);    // doesn't return
  }

  // if "fire" button down at boot, increment filenumber to play
  if (configuring)
  {
    if (++Data.FileNumber > MAX_FILE_NUMBER)
    {
      Data.FileNumber = DEFAULT_FILE_NUMBER;
    }

    save_eeprom_data();
  }

  // Set volume, ensure player state is STOPPED
  player.volume(Data.Volume);
  PlayerState = STATE_STOPPED;

  // announce
  debugf("\nReady: %s\n", VERSION);
  debugf("File %d, volume %d\n", Data.FileNumber, Data.Volume);
}


//*********************************************************
// Run the player.
//*********************************************************

void loop()
{
  // poll the button, get button state, up or down (HIGH or LOW)
  button.loop();
  uint8_t button_state = button.getState();

  // check button state and player state to see what to do
  if (button_state == LOW)
  {
    // button down
    if (PlayerState == STATE_STOPPED)
    {
      player.play(Data.FileNumber);
      PlayerState = STATE_PLAYING;
    }
  }
  else
  {
    // button up
    if (PlayerState == STATE_PLAYING)
    {
      player.stop();
      PlayerState = STATE_STOPPED;
    }
  }
}

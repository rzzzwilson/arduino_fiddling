//
// Exercise the SoftSwitch board.
// Connect these before testing:
//   Uno  -> SoftSwitch
//   ------------------
//   GND     GND
//   5v      Vout
//   pin11   SENSE
//   pin12   HOLD
//
// and power:
//   PS   -> SoftSwitch
//   ------------------
//   8volt   Vin
//   GND     GND
//

// Define the pins for SENSE, HOLD and LED
const int HOLD = 12;
const int SENSE = 11;
const int LED = LED_BUILTIN;

// data for the LED flasher
unsigned long last_flash = 0;
const unsigned long flash_interval = 1000;

// time to wait before power off
const unsigned long off_wait = 1500;


void flash_led(int num, int period)
{
  for (int i=0; i < num; ++i)
  {
    digitalWrite(LED, HIGH);
    delay(period);
    digitalWrite(LED, LOW);
    delay(period);
  }
}

int read_button(int pin)
{
  byte states = 0;
  
  while (true)
  {
    states = (states << 1) + (digitalRead(pin) & 1);

    if (states == 0xff || states == 0x00)
    {
      return states == 0xff ? HIGH : LOW;
    }

    delay(20);
  }
}

void setup(void)
{
  // set SENSE and HOLD modes, turn on power HOLD
  pinMode(HOLD, OUTPUT);
  digitalWrite(HOLD, HIGH);
  pinMode(LED, OUTPUT);
  digitalWrite(LED, HIGH);

  pinMode(SENSE, INPUT_PULLUP);
  while (read_button(SENSE) == LOW)
  {
  }
}

void loop(void)
{
  unsigned long now = millis();

  // flash the LED
  if (now - last_flash >= flash_interval)
  {
    last_flash = now;
    digitalWrite(LED, !digitalRead(LED));
  }
  
  // check the SENSE button
  if (read_button(SENSE) == LOW)
  {
    // tell the world we are preparing to shut down
    // abort shutdown if button released
    while (millis() - now < off_wait)
//    for (int i = 0; i < 10; ++i)
    {
      flash_led(1, 75);
      if (read_button(SENSE) == HIGH)
      {
        return;
      }
    }

    // set LED HIGH, turn off HOLD
    digitalWrite(LED, HIGH);
    digitalWrite(HOLD, LOW);

    // then wait here forever
    while (1)
    {
    }
  }
}

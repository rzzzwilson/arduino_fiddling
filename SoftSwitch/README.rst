SoftSwitch
==========

A little bit of arduino code to latch the hardware softswitch circuit and
detect another button press thereby turning the power OFF.

SoftSwitch Limitations
----------------------

Input voltage range:    7 ~ 20v
Max input current:      1.0A

/*

Test code to read the NMEA message stream from a QRP-Labs QLG1 GPS board.

 */
 
#include <SoftwareSerial.h>

SoftwareSerial mySerial(2, 3); // RX, TX

void setup()
{
  // Open serial communications and wait for port to open:
  Serial.begin(115200);
  
  // wait for serial port to connect. Needed for Native USB only
  while (!Serial)
  {
    ; 
  }

  // set the data rate for the SoftwareSerial port (GPS)
  mySerial.begin(9600);

  Serial.println("\nReading GPS!");
}

// buffer in which to assemble messages
char buffer[80];
int buff_index = 0;
  
void loop()
{
  while (mySerial.available())
  {
    char ch = mySerial.read();
    
    if (ch == '\n')
    {
      buffer[buff_index] = '\0';
      buff_index = 0;
      Serial.println(buffer);
    }
    else
    {
      if (buff_index < sizeof(buffer) - 1)
          buffer[buff_index++] = ch;
    }
  }
}

//For more Projects: www.arduinocircuit.com
// Gnd - GND
// 3.3v - VCC
// 3.3v - CS
// Analog 4 - SDA
// Analog 5 - SLC
#include <Wire.h>
//Device address
const int DEVICE_ADDRESS = 0x53;

//ADXL345 register addresses
char POWER_CTL = 0x2D;
char DATA_FORMAT = 0x31;
char DATAX0 = 0x32; //X-Axis Data 0
char DATAX1 = 0x33; //X-Axis Data 1
char DATAY0 = 0x34; //Y-Axis Data 0
char DATAY1 = 0x35; //Y-Axis Data 1
char DATAZ0 = 0x36; //Z-Axis Data 0
char DATAZ1 = 0x37; //Z-Axis Data 1
void setup()
{
  Serial.begin(115200);
  delay(2500);
  Serial.println("Started");
  
  Wire.begin();
  writeTo(DEVICE_ADDRESS, DATA_FORMAT, 0x01); //Set ADXL345 to +- 4G
  writeTo(DEVICE_ADDRESS, POWER_CTL, 0x08); //Put the ADXL345
}

void loop()
{
  readAccel(); //Read acceleration x, y, z
  delay(1000);
}

void readAccel()
{
  //Read the data
  uint8_t numBytesToRead = 6;
  byte _buff[numBytesToRead];
  
  readFrom(DEVICE_ADDRESS, DATAX0, numBytesToRead, _buff);
  
  //Read the values ​​from the register and convert to int (Each axis has 10 bits, in 2 Bytes LSB)
  int x = (((int)_buff[1]) << 8) | _buff[0];
  int y = (((int)_buff[3]) << 8) | _buff[2];
  int z = (((int)_buff[5]) << 8) | _buff[4];
  
  Serial.print("x=");
  Serial.print(x);
  Serial.print(", y=");
  Serial.print(y);
  Serial.print(", z=");
  Serial.println(z);
}

//Auxiliary writing function
void writeTo(int device, byte address, byte val)
{
  Wire.beginTransmission(device);
  Wire.write(address);
  Wire.write(val);
  Wire.endTransmission();
}

//Auxiliary reading function
void readFrom(int device, byte address, int num, byte _buff[])
{
  Wire.beginTransmission(device);
  Wire.write(address);
  Wire.endTransmission();
  Wire.beginTransmission(device);
  Wire.requestFrom(device, num);
  int i = 0;
  while(Wire.available())
  {
    _buff[i] = Wire.read();
    i++;
  }
  Wire.endTransmission();
}

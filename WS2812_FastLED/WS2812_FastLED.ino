/*
 * Test code to exercise an 8 LED WS2812 strip.
 * Connections:
 *    5v to Vcc
 *    GND to GND
 *    DIN to pin 6 (Uno), pin D7 Iota (through 220ohm resistor)
 *                        pin 18 on WiFi_Kit_32
 */

#include <FastLED.h>

//#define LED_PIN     6
#define LED_PIN     SCK  // pin 18
#define NUM_LEDS    8
CRGB leds[NUM_LEDS];

#define MinBrightness  1
#define MaxBrightness  64
#define StepBrightness 1

//int Brightness = MinBrightness;
int Brightness = MinBrightness;
int DeltaBrightness = StepBrightness;

void setup()
{
  FastLED.addLeds<WS2812, LED_PIN, GRB>(leds, NUM_LEDS);

  leds[0] = CRGB(255, 0, 0);
  leds[1] = CRGB(0, 255, 0);
  leds[2] = CRGB(0, 0, 255);
  leds[3] = CRGB(255, 127, 0);
  leds[4] = CRGB(255, 0, 127);
  leds[5] = CRGB(127, 255, 0);
  leds[6] = CRGB(0, 255, 127);
  leds[7] = CRGB(255, 255, 255);
  
  FastLED.setBrightness(Brightness);
  
  FastLED.show();
  delay(1000);
}

void loop()
{
  CRGB tmp = leds[NUM_LEDS - 1];

  FastLED.setBrightness(Brightness);
  
  Brightness += DeltaBrightness;
  if (Brightness > MaxBrightness)
  {
    Brightness = MaxBrightness;
    DeltaBrightness = -DeltaBrightness;
  }
  else if (Brightness < MinBrightness)
  {
    Brightness = MinBrightness;
    DeltaBrightness = -DeltaBrightness;
  }

  for (int i = NUM_LEDS - 1; i >= 1; --i)
  {
    leds[i] = leds[i-1];
  }
  leds[0] = tmp;
  FastLED.show();
  delay(250);
}

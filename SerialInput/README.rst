This is just a small bit of code to show how to respond to text input
from the Serial input while doing something else.

The code blinks the builtin LED (without delay) and gathers text commands
from the serial input into a buffer.  When a "\\n" character is received
the text input is echoed back to the sender.

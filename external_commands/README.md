# Arduino External Commands

The code here is a simple demonstration of one way to receive commands
typed into the Arduino Serial Monitor and action them on an Arduino board.

The demonstration code just echoes what was sent and shows how to split
the command string into a *prefix* character and a *suffix* string.
If the prefix character is 'X' the suffix string is converted to an
integer value which is also returned.

This code was written and tested on a Uno board, but should run on any
board.

## Test

The transcript below shows a test run:

    Key in command followed by ENTER.
    Command: 'test'
    prefix='t', suffix='est', len suffix=3
    Command: '123'
    prefix='1', suffix='23', len suffix=2
    Command: 'Xabc'
    prefix='X', suffix='abc', len suffix=3
    X: integer value=0
    Command: 'X123'
    prefix='X', suffix='123', len suffix=3
    X: integer value=123 
    Command: 'X456Q'
    prefix='X', suffix='456Q', len suffix=4
    X: integer value=456

## Not using Arduino Strings

Note that the code doesn't use the C++ *String* class.  It's best not
to use that on limited memory microcontrollers since it can lead to 
"out of memory" problems if fragmentation occurs.

https://hackingmajenkoblog.wordpress.com/2016/02/04/the-evils-of-arduino-strings/

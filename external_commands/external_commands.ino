//--------------------------------------------------------------
// Demonstration of buffering and execution of external commands
// received from the Serial connection.
//--------------------------------------------------------------

#define MAX_COMMAND_LEN   10    // max chars in buffer, includes terminator
#define COMMAND_END_CHAR  '\n'  // the command terminator character

char CommandBuffer[MAX_COMMAND_LEN+1];  // assemble command here
int CommandIndex = 0;     // index into CommandBuffer for next char

//--------------------------------------------------------------
// Handle a command.
//
//     cmd  the address of a C string holding the command
//
// The string will not be zero length.
//--------------------------------------------------------------

void handle_cmd(char *cmd)
{
  // just echo back, as before
  Serial.print("Command: '");
  Serial.print(CommandBuffer);
  Serial.println("'");

  // now analyze the command string
  char prefix = *cmd;
  char *suffix = cmd + 1;
  int len_suffix = strlen(suffix);

  Serial.print("prefix='");
  Serial.print(prefix);
  Serial.print("', suffix='");
  Serial.print(suffix);
  Serial.print("', len suffix=");
  Serial.println(strlen(suffix));

  // show how to get an integer value from a suffix
  // note that we only handle uppercase X, maybe you want to convert buffer to upper/lower case
  if (prefix == 'X')
  {
    // note: if "suffix" isn't a number, you get 0.  Try "Xabc".
    int value = atoi(suffix);
    
    Serial.print("X: integer value=");
    Serial.println(value);
  }
  
  // if you have lots of commands you would probably do something like
  // this, rather than the "if (prefix == 'X')" code above
  //
  //     switch (prefix)
  //     {
  //       case 'X':
  //         do_xcmd(suffix);
  //         break;
  //     // etc
  //     }
}

//--------------------------------------------------------------
// Gather external command chars, if any.
//
// If a complete command has been received, execute it.
//--------------------------------------------------------------

void do_commands(void)
{
  bool overflow = false;    // "true" if command buffer overflowed

  // gather any commands from the external controller
  while (Serial.available())
  {
    char ch = Serial.read();

    if (CommandIndex < MAX_COMMAND_LEN)
    {
      // only add if command not over-length
      CommandBuffer[CommandIndex++] = ch;
    }
    else
      overflow = true;

    if (ch == COMMAND_END_CHAR)   // if end of command, execute it
    {
      if (overflow)
      {
        overflow = false;
        Serial.println("TOO LONG");
      }
      else
      {
        CommandBuffer[CommandIndex-1] = '\0'; // convert to C string, remove terminator

        // handle the command string
        if (strlen(CommandBuffer))    // if there is something to execute
        {
          handle_cmd(CommandBuffer);
        }
      }
        
      CommandIndex = 0; // prepare for next command
    }
  }
}

void setup()
{
  Serial.begin(115200);
  delay(2500);    // wait for Serial to initialize
  Serial.println("Key in command followed by ENTER.");
}

void loop()
{
  // execute any external commands
  do_commands();

  // whatever you want here
}

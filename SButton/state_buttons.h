#ifndef BUTTONS_H

#include <Arduino.h>

// types for library code
typedef unsigned long uint32_t;
typedef unsigned char uint8_t;

// possible button states
typedef enum
{
    BUTTON_UNKNOWN,
    BUTTON_LOW,
    BUTTON_RISE,
    BUTTON_HIGH,
    BUTTON_FALL
} button_state;

// struct for handling one button
typedef struct
{
    uint32_t past_reads;    // last 32 reads of the pin
    uint8_t prev_state;     // previous physical state
    byte pin;               // the pin # the button is attached to
} Button;

// definitions of functions in library
void * btn_add(byte pin_number);
button_state btn_poll(void * token);
char *btn_decode(button_state state);

#endif

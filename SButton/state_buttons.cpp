/*
 * Library to poll and debounce multiple buttons connected
 * from pin to ground.  Functions:
 *     void btn_init(byte num_buttons)
 *         initialize system for "num_buttons" buttons
 *     byte btn_add(byte pin_number)
 *         add pin "pin_number" to system
 *         returns index of pin, used to poll
 *     button_state btn_poll(byte index)
 *         given pin number "index", returns button state;
 *             BUTTON_LOW   state was LOW, now LOW
 *             BUTTON_RISE  state was LOW, now HIGH
 *             BUTTON_HIGH  state was HIGH, now HIGH
 *             BUTTON_FALL  state was HIGH, now LOW
 * 
 * There is small delay at the end og the btn_poll() code.
 * If the loop() code does more than just poll buttons you
 * can try removing the small delay.  Using a single very
 * noisy "test" button I needed the delay.
 */

#include "state_buttons.h"


/**********************************************
  Add a button to the system.
  "pin_number" is the physical pin number.
  Returns a "token" used to access the button.
  Allocates memory for each button state.
 **********************************************/

void *btn_add(byte pin_number)
{
    Button *slot = (Button*) malloc(sizeof(Button));
    
    // set pin to INPUT_PULLUP
    pinMode(pin_number, INPUT_PULLUP);

    // set button state
    slot->pin = pin_number;
    slot->prev_state = digitalRead(pin_number);
    // slot->prev_state holds last 32 read states
    if (slot->prev_state)
        slot->past_reads = 0xffffffff;
    else
        slot->past_reads = 0x00000000;

    // return "token" to user
    return (void*) slot;
}

/**********************************************
  Poll the button specified by the user.
  Returns one of:
        BUTTON_LOW, BUTTON_RISE,
        BUTTON_HIGH, BUTTON_FALL
 **********************************************/

button_state btn_poll(void *token)
{
    Button *slot = (Button*) token;
    byte curr_state = HIGH;

    slot->past_reads = (slot->past_reads << 1) | digitalRead(slot->pin);

    if (slot->past_reads == 0x00000000)
    {
        curr_state = LOW;
    }
    else if (slot->past_reads == 0xffffffff)
    {
        curr_state = HIGH;
    }
  
    // quick compare of previous and current states
    button_state result = BUTTON_UNKNOWN;
  
    switch ((slot->prev_state << 8) + curr_state)
    {
        case ((LOW << 8) + LOW):   result = BUTTON_LOW;  break;
        case ((LOW << 8) + HIGH):  result = BUTTON_RISE; break;
        case ((HIGH << 8) + HIGH): result = BUTTON_HIGH; break;
        case ((HIGH << 8) + LOW):  result = BUTTON_FALL; break;
    }
  
    // we must remember the (new) previous state
    slot->prev_state = curr_state;

    // if you have almost nothing in the loop() function you might get 
    // button noise unless the small delay below is used
    delayMicroseconds(500);

    return result;
}

/**********************************************
  Convert enum value into printable string.
 **********************************************/

char *btn_decode(button_state state)
{
    switch (state)
    {
        case BUTTON_UNKNOWN: return (char*) "BUTTON_UNKNOWN";
        case BUTTON_LOW: return (char*) "BUTTON_LOW";
        case BUTTON_RISE: return (char*) "BUTTON_RISE";
        case BUTTON_HIGH: return (char*) "BUTTON_HIGH";
        case BUTTON_FALL: return (char*) "BUTTON_FALL";
    }
    return (char*) "?UNKNOWN?";
}

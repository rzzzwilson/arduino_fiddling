# SButton

A library to handle multiple pushbuttons and get full "state" returned.
This means you add a button to the system and when polling each button
you get back one of these 4 states:
    BUTTON_LOW   the button pin is LOW and was LOW previously
    BUTTON_RISE  the button pin is HIGH and was LOW previously
    BUTTON_HIGH  the button pin is HIGH and was HIGH previously
    BUTTON_FALL  the button pin is LOW and was HIGH previously

This library is a standard non-C library.  See ../StateButton for the
C++ style library.

*Bounce.ino* exercises the library.  Connect a pushbutton from Arduino
pin 5 to ground before running the code.


/*
  Debounce

*/

#include "state_buttons.h"


// define the pin the button is connected to
const byte buttonPin = 5;   // the number of the pushbutton pin

// save for the token returned by btn_add()
void *test_button;         // button token for "buttons" library

// previous printed state
// use this to only print state changes
button_state last_print_state = BUTTON_UNKNOWN;


void setup()
{
    Serial.begin(115200);

    // initialize the button
    test_button = btn_add(buttonPin);

    // set "last printed value" to current value
    last_print_state = btn_poll(test_button);

    // on Atmega32U4 chips we need to wait
    while (true)
    {
        if (Serial)
            break;
        delay(100);
    }
    Serial.println("OK");
}

void loop()
{
    // current state of the button
    button_state curr_state = btn_poll(test_button);
  
    if (last_print_state != curr_state)
    {
        // only print if state has changed
        Serial.print("button: ");
        Serial.print(millis());
        Serial.print("  ");
        Serial.println(btn_decode(curr_state));
    }

    // update last printed state
    last_print_state = curr_state;
}

This directory contains programs to convert C code to an image file format
and vice-versa.

#image2c.py

Program to convert an image file (PNG, etc) to a C file of e-paper data.

Usage: image2c.py [-h] [-b|-r] [-s<size>] <image_file> [<output_file>]

where -h             prints help and stops
      -b             emit only black data
      -r             emit only red data
      <size>         size of the output file (-swxh, eg, -s212x104)
      <image_file>   path to image file to read
      <output_file>  path to output file
                     ('stdout' if not defined)

#c2image.py

Program to convert a single array of C pixel data to an image file.

Usage:  data2image.py  <data_file> <image_file>


#!/usr/bin/env python3

"""
Take a dataset describing an image and create an image file.

Usage:  data2image.py  <data_file> <image_file>

Assume image size is 104x212.
"""

import sys
from PIL import Image


def read_data(fpath):
    """Read data from given file, return as list of bytes values."""

    with open(fpath, 'r') as f:
        data = f.read()

    data = data.replace('\n', '')
    data = data.split(',')
    data = [int(v, 16) for v in data]
    print(data)
    print(f'len(data)={len(data)}, len(data)*8={len(data)*8}, 104x212={104*212}')

    return data

def bytes2bits(data):
    """Takes a list of byte values and returns iterator  - one bit at a time."""

    for b in data:
        for _ in range(8):
            yield b & 0x80
            b = b << 1

def write_image(impath, data):
    """Write data to the image file."""

    image = Image.new('1', (104, 212), color='white')
    new_data = []
    for bit in bytes2bits(data):
        if bit:
            new_data.append(0)
        else:
            new_data.append(255)
    image.putdata(new_data)
    image.save(impath)


if len(sys.argv) != 3:
    print('Usage:  data2image.py  <data_file> <image_file>')
    sys.exit(10)

data_file = sys.argv[1]
image_file = sys.argv[2]

data = read_data(data_file)
print(f'HUH: {len(data)}')
write_image(image_file, data)

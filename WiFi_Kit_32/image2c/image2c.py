#!/usr/bin/env python3

"""
Program to convert an image file (PNG, etc) to a C file of e-paper data.
Produces either both or one of black and red data arrays.

Usage: image2c.py [-h] [-b|-r] [-s<size>] <image_file> [<output_file>]

where -h             prints help and stops
      -b             emit only black data (default is both)
      -r             emit only red data (default is both)
      <size>         size of the output file (-swxh, eg, -s212x104)
      <image_file>   path to image file to read
      <output_file>  path to output file ('stdout' if not defined)
"""

import sys
import getopt
import os.path
from datetime import datetime
from PIL import Image


ProgName = 'image2c.py'
DefaultWidth = 104
DefaultHeight = 212


EmitCount = 0
EmitAccum = 0
LineCount = 0

def emit(f, bit):
    global EmitCount, EmitAccum, LineCount

    EmitAccum = (EmitAccum << 1) + bit
    EmitCount += 1
    if EmitCount >= 8:
#        # "invert" bits
#        bin_str = f'{EmitAccum:08b}'
#        rev_bin_str = bin_str[::-1]
#        value = int(rev_bin_str, 2)
#        f.write(f'0x{value:02x}, ')
        f.write(f'0x{EmitAccum:02x}, ')
        EmitAccum = 0
        EmitCount = 0
        LineCount += 1
        if LineCount >= 8:
            f.write('\n')
            LineCount = 0

def image2c(input_file, output_file, size, only_colour):
    """Convert image data in 'input_file' to C code in 'output_file'.

    input_file   the input filename
    output_file  the open output file
    size         (width, height) of output image data (pixels)
    only_colour  'None' if all colours (red + black), otherwise
                 'red' for red only and 'black' for black only.

    'Invert' the byte values in each line.
    """

    global EmitCount, EmitAccum, LineCount

    # datetime object containing current date and time
    now = datetime.now()
    dt_string = now.strftime("%d %b %Y %H:%M:%S")

    # write some info into the 'C' output file
    output_file.write(f'// Written by {ProgName} from {input_file}.\n')
    output_file.write(f'// {dt_string}\n\n')

    # open image, resize if necessary
    image = Image.open(input_file)

    if image.size != size:
        # resize the image
        print(f'Resizingi from {image.size}')
        image = image.resize(size)

    # unpack width and height, get image data
    (width, height) = image.size
    pixels = list(image.getdata())

    # check the number of colour bands
    bands = image.getbands()
    num_bands = len(bands)

    if num_bands not in (3, 4):
        print(f'Sorry, image file must be RGB or RGBA, got {bands}.')
        return 10

    # convert to 3 band if it's 4
    if bands == ('R', 'G', 'B', 'A'):
        pixels = [(r, g, b) for (r, g, b, _) in pixels]

    # get stem input filename
    (input_file_stem, _) = os.path.splitext(os.path.basename(input_file))

    # cycle through pixels, save black channel, if required
    if only_colour != 'red':
        EmitCount = 0
        EmitAccum = 0
        LineCount = 0

        output_file.write(f'const unsigned char {input_file_stem}_bits_b[] = {{\n')
        data = []
        for (r, g, b) in pixels:
            if r == 0 and g == 0 and b == 0:    # ony handle black pixels
                # a black pixel
                emit(output_file, 1)
            else:
                emit(output_file, 0)

        output_file.write('};\n')

    # cycle through pixels, save red channel, if required
    if only_colour != 'black':
        EmitCount = 0
        EmitAccum = 0
        LineCount = 0

        output_file.write('\n')
        output_file.write(f'const unsigned char {input_file_stem}_bits_r[] = {{\n')
        data = []
        for (r, g, b) in pixels:
            print(f'r={r}, g={g}, b={b}, ', end='')
            if r > 240 and g < 128 and b < 128:
                # a red pixel
#                data.append(1)
                emit(output_file, 1)
                print('RED!')
            else:
#                data.append(0)
                emit(output_file, 0)
                print('NOT')

#        # now get 8 bit values
#        accum = 0
#        bit_count = 0
#        field_count = 0
#        for bit in data:
#            accum = (accum << 1) + bit
#            bit_count += 1
#            if bit_count >= 8:
#                output_file.write(f'0x{accum:02x}, ')
#                accum = 0
#                bit_count = 0
#                field_count += 1
#                if field_count >= 8:
#                    output_file.write('\n')
#                    field_count = 0
#        if accum:
#            output_file.write(f'0x{accum:02x}, ')
        output_file.write('};\n')

#        # now "invert" each 8 bit value and write out
#        count = 0
#        for bd in byte_data:
#            bin_str = f'{bd:08b}'
#            rev_bin_str = bin_str[::-1]
#            value = int(rev_bin_str, 2)
#            output_file.write(f'0x{value:02x}, ')
#            count += 1
#            if count >= 8:
#                output_file.write('\n')
#                count = 0
#        bin_str = f'{byte_data[-1]:08b}'
#        rev_bin_str = bin_str[::-1]
#        value = int(rev_bin_str, 2)
#        output_file.write(f'0x{value:02x}}};')

    return 0

def usage(msg=None):
    """Print usage doc and optional message."""

    if msg:
        print('*' * 60)
        print(msg)
        print('*' * 60)
    print(__doc__)

def main():
    """Prepare environment and then call image2c{}."""

    # ponder on commandline args
    # image2c.py [-h] [-b|-r] [-s<size>] <image_file> [<output_file>]
    ProgName = sys.argv[0]
    try:
        (opts, args) = getopt.gnu_getopt(sys.argv, 'hbrs:',
                                         ['help', 'black', 'red', 'size='])
    except getopt.GetoptError:
        usage()
        sys.exit(10)

    only_colour = None
    size = (DefaultWidth, DefaultHeight)
    output_file = sys.stdout

    for opt, arg in opts:
        if opt in ('-h', '--help'):
            usage()
            sys.exit(0)
        elif opt in ('-b', '--black'):
            if only_colour:
                usage(f'Colour already set: {only_colour}')
                sys.exit(1)
            only_colour = 'black'
        elif opt in ('-r', '--red'):
            if only_colour:
                usage(f'Colour already set: {only_colour}')
                sys.exit(1)
            only_colour = 'red'
        elif opt in ('-s', '--size'):
            size = arg.split('x')
            if len(size) != 2:
                usage('--size arg is wrong: {arg}')
                sys.exit(3)
            size = tuple(int(s) for s in size)

    if len(args) not in (2, 3):
        usage()
        sys.exit(2)

    input_file = args[1]
    try:
        with open(input_file) as f:
            pass
    except IOError:
        print(f"Sorry, can't find file '{input_file}'")
        sys.exit(10)

    if len(args) == 3:
        try:
            output_file = open(args[2], 'w')
        except IOError:
            print(f"Sorry, can't open file '{args[2]}' for output")
            sys.exit(10)

    image2c(input_file, output_file, size, only_colour)


sys.exit(main())

// base class GxEPD2_GFX can be used to pass references or pointers to the display instance as parameter, uses ~1.2k more code
// enable or disable GxEPD2_GFX base class
//#define ENABLE_GxEPD2_GFX 0

#include <GxEPD2_3C.h>
#include "heltec.h"

// connections to the IL0373F display
#define DC 17
#define SDI 23
#define CS 5
#define CLK 18
#define BUSY 4
// no RST, use -1
#define RST -1


// declare the e-paper display
//GxEPD2_3C<GxEPD2_213c, GxEPD2_213c::HEIGHT> display(GxEPD2_213c(SS, 17, 16, 4));
GxEPD2_3C<GxEPD2_213c, GxEPD2_213c::HEIGHT> display(GxEPD2_213c(SS, 17, RST, 4));

// buffer to build strings to display
char DisplayBuffer[100];


void setup()
{
  Serial.begin(115200);
  Serial.println();
  Serial.println("setup");
  delay(100);
  display.init(115200);

  // initialize the OLED display
  Heltec.begin(true /*DisplayEnable Enable*/, false /*LoRa Enable*/, true /*Serial Enable*/);
  Heltec.display->clear();
  Heltec.display->drawString(0, 0, "Starting...");
  sprintf(DisplayBuffer, "width=%d", display.width());
  Heltec.display->drawString(0, 10, DisplayBuffer);
  sprintf(DisplayBuffer, "height=%d", display.height());
  Heltec.display->drawString(0, 20, DisplayBuffer);
  Heltec.display->display();
  delay(1000);
}

void loop()
{
  drawXyzzy();
  delay(10000);
}

extern unsigned char hanko_bits_b[];
extern unsigned char hanko_bits_r[];

void drawXyzzy()
{
  display.setRotation(0);
  
  // show what we are doing on OLED screen
  Heltec.display->clear();
  Heltec.display->drawString(0, 0, "drawXyzzy");
  sprintf(DisplayBuffer, "width=%d", display.epd2.WIDTH);
  Heltec.display->drawString(0, 10, DisplayBuffer);
  sprintf(DisplayBuffer, "height=%d", display.epd2.HEIGHT);
  Heltec.display->drawString(0, 20, DisplayBuffer);
  Heltec.display->drawString(0, 30, "Black, White & Red");
  Heltec.display->display();

  display.firstPage();
  do
  {
    display.fillScreen(GxEPD_WHITE);
    display.drawBitmap(0, 0, hanko_bits_b, display.epd2.WIDTH, display.epd2.HEIGHT, GxEPD_BLACK);
    display.drawBitmap(0, 0, hanko_bits_r, display.epd2.WIDTH, display.epd2.HEIGHT, GxEPD_RED);
  }
  while (display.nextPage());

  delay(10000);
}

// E-ink display IL0373F

#include <GxEPD.h>
#include <GxGDEW0213Z16/GxGDEW0213Z16.h>
#include <GxIO/GxIO_SPI/GxIO_SPI.h>
#include <GxIO/GxIO.h>
//#include <Fonts/FreeMonoBold9pt7b.h>
#include <Fonts/FreeSansBold9pt7b.h>

// connections to the IL0373F display
#define DC 17
#define SDI 23
#define CS 5
#define CLK 18
#define BUSY 4
// no RST, use -1
#define RST -1

// create object for communication ibterface
GxIO_Class io(SPI, CS, DC, RST);
// create a display object
GxEPD_Class eink(io, RST, BUSY);

void setup() {
  // initialization of communication with the display
  eink.init();
  // display rotation setting 0-3
  eink.setRotation(1);
  // set the font used for writing text
//  eink.setFont(&FreeMonoBold9pt7b);
  eink.setFont(&FreeSansBold9pt7b);
}

void loop()
{
  static unsigned int previous_millis = 0;
  
  // set white display background
  eink.fillScreen(GxEPD_WHITE);
  // set text color
  eink.setTextColor(GxEPD_BLACK);
  // set the cursor to 0.0
  eink.setCursor(0, 0);
  // line feed to print the first line correctly
  eink.println();
  // print text with line feed
  eink.println("  ->  Arduino navody  <-");
  eink.println("Start time: ");
  // set red color for text
  eink.setTextColor(GxEPD_RED);
  // print number - time since start
//  eink.print(millis() / 1000);
  eink.print(previous_millis / 1000);
  eink.println(" s.");
  // transcription of text on the display - according to the amount of text
  // this command takes units up to tens of seconds
  eink.update();
  // printing a solid square in black,
  // coordinates are the maximum width and height minus 25 pixels
  // dimensions of the shape are 24x24 pixels
  eink.fillRect(eink.width() - 50, eink.height() - 50, 49, 49, GxEPD_RED);
  eink.update();
  // to show you how to print and rewrite the new time to existing content
  eink.print(millis() / 1000);
  eink.println(" s.");
  eink.update();
  previous_millis = millis();
  // pause before new during loop
//  delay(5000);
}

// E-ink display IL0373F
// Draw lines

#include <GxEPD.h>
#include <GxGDEW0213Z16/GxGDEW0213Z16.h>
#include <GxIO/GxIO_SPI/GxIO_SPI.h>
#include <GxIO/GxIO.h>
#include "heltec.h"

// connections to the IL0373F display
#define DC 17
#define SDI 23
#define CS 5
#define CLK 18
#define BUSY 4
// no RST, use -1
#define RST -1

// create object for communication ibterface
GxIO_Class io(SPI, CS, DC, RST);
// create a display object
GxEPD_Class eink(io, RST, BUSY);

// buffer to build strings to display
char DisplayBuffer[100];


void setup()
{
  // initialization of communication with the display
  eink.init();
  eink.setRotation(3);

  // initialize OLED display
  Heltec.begin(true /*DisplayEnable Enable*/, false /*LoRa Enable*/, true /*Serial Enable*/);
  Heltec.display->clear();
  Heltec.display->drawString(0, 0, "Starting...");
  sprintf(DisplayBuffer, "width=%d", eink.width());
  Heltec.display->drawString(0, 10, DisplayBuffer);
  sprintf(DisplayBuffer, "height=%d", eink.height());
  Heltec.display->drawString(0, 20, DisplayBuffer);
  Heltec.display->display();
}

uint16_t next_colour(uint16_t prev)
{
  if (prev == GxEPD_BLACK)
    return GxEPD_WHITE;
  if (prev == GxEPD_WHITE)
    return GxEPD_RED;
  if (prev == GxEPD_RED)
    return GxEPD_BLACK;
  return GxEPD_BLACK;
}

void loop()
{
  static uint16_t last = GxEPD_WHITE;
  long start = millis();

  Heltec.display->clear();

  for (int i = 0; i < 11; ++i)
  {
    eink.fillRect(i*5, i*5, eink.width()-i*10, eink.height()-i*10, last);
    last = next_colour(last);
  }
//  eink.drawLine(1,2,3,4);
  
  int delta = millis() - start;
  sprintf(DisplayBuffer, "%d ms to draw", delta);
  Heltec.display->drawString(0, 0, DisplayBuffer);
  
  eink.update();
  
  delta = millis() - start;
  sprintf(DisplayBuffer, "%d ms to update", delta);
  Heltec.display->drawString(0, 10, DisplayBuffer);
  Heltec.display->display();

  delay(5000);
}

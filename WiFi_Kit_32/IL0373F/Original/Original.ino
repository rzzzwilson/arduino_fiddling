// E-ink displej IL0373F

// připojení potřebných knihoven pro displej
#include <GxEPD.h>
#include <GxGDEW0213Z16/GxGDEW0213Z16.h>
#include <GxIO/GxIO_SPI/GxIO_SPI.h>
#include <GxIO/GxIO.h>
#include <Fonts/FreeMonoBold9pt7b.h>
// nastavení čísel propojovacích pinů
#define DC 17
#define SDI 23
#define CS 5
#define CLK 18
#define BUSY 4
 // displej reset nemá, proto knihovně předáme -1
#define RST -1

// vytvoření objektu io pro komunikační rozhraní
GxIO_Class io(SPI, CS, DC, RST);
// vytvoření objektu displeje
GxEPD_Class eink(io, RST, BUSY);

void setup() {
  // inicializace komunikace s displejem
  eink.init();
  // nastavení rotace displeje 0-3
  eink.setRotation(1);
  // nastavení fontu použitého pro psaní textu
  eink.setFont(&FreeMonoBold9pt7b);
}

void loop() {
  // nastavení bílého pozadí displeje
  eink.fillScreen(GxEPD_WHITE);
  // nastavení barvy textu
  eink.setTextColor(GxEPD_BLACK);
  // nastavení kurzoru na souřadnice 0,0
  eink.setCursor(0, 0);
  // odřádkování pro správné vytištění prvního řádku
  eink.println();
  // tisk textu s odřádkováním
  eink.println("->Arduino navody<-");
  eink.println("Cas od spusteni: ");
  // nastavení červené barvy pro text
  eink.setTextColor(GxEPD_RED);
  // vytištění čísla - času od spuštění
  eink.print(millis() / 1000);
  eink.println(" s.");
  // přepis textu na displej - dle množství textu
  // tento příkaz zabere jednotky až desítky sekund
  eink.update();
  // pro ukázku vytištění a přepis nového času ke stávajícímu obsahu
  eink.print(millis() / 1000);
  eink.println(" s.");
  eink.update();
  // pro ukázku vytištění a přepis nového času ke stávajícímu obsahu
  eink.print(millis() / 1000);
  eink.println(" s.");
  eink.update();
  // vytištění plného čtverce černou barvou,
  // souřadnice jsou maximální šířka a výška mínus 25 pixelů,
  // rozměry obrazce jsou 24x24 pixelů
  eink.fillRect(eink.width() - 25, eink.height() - 25, 24, 24, GxEPD_BLACK);
  eink.update();
  // pauza před novým během smyčky
  delay(5000);
}

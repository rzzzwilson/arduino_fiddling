# Experimental testing of the touchpad board.

Doc on the capacitive sensor library is at:

    https://www.pjrc.com/teensy/td_libs_CapacitiveSensor.html

## Results

The "interleaved" pad works fine.  The "solid" pad doesn't work at all.

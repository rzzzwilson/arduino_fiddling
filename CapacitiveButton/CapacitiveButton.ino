#include <CapacitiveSensor.h>

int led = LED_BUILTIN;

// pins used for capacitive pad
const int send_pin = 7;   // common to all pads
const int recv_pin1 = 6;  // sense pin for pad1

CapacitiveSensor cap_pad1 = CapacitiveSensor(send_pin, recv_pin1);

void setup()                    
{
  Serial.begin(115200);
  Serial.println("OK");
  
  pinMode(led, OUTPUT);
  digitalWrite(led, LOW);
}

void loop()                    
{
  long state1 = cap_pad1.capacitiveSensor(30);
  digitalWrite(led, state1 > 40);
    
  delay(10);
}

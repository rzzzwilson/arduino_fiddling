How to manage EEPROM initialization.
====================================

When storing values in EEPROM there often arises the question::

    How do I initialize the EEPROM to sane values?

One approach is to write a special "initialize" sketch that just writes
the appropriate values into EEPROM.  Then you load the actual sketch that
uses the initialized EEPROM values.

Another, similar, idea is to have code in the arduino sketch that initializes
EEPROM data.  Whether the code is called or not is controlled by a #define.
You must be careful to turn off the #define otherwise the EEPROM gets initialized
every time the sketch boots.

The code here uses a different approach.  An extra field is created alongside the
values to be stored in EEPROM.  This field contains the CRC checksum of the EEPROM
values.  Every time the EEPROM values are updated the CRC checksum is recalculated
and also stored in EEPROM.  When a sketch starts it calculates the CRC checksum
of the values in EEPROM and compares that with the stored checksum.  If they are
the same the EEPROM data is valid.  If they are not the same then the EEPROM values
must be initialized to sane starting values.

Sample code
-----------

In the sample sketch *eeprom_init.ino* we create a struct holding values that will
be stored in EEPROM.  The code reads and writes the entire struct to EEPROM.  The
CRC is updated whenever the struct is written to EEPROM.

The struct just holds a "number of boots" value that is incremented every time the
sketch starts.  Any data you want can be stored in the struct.

When first running the sketch you should see that the sketch has decided to
initialize the EEPROM and the boot number stored is set to 0.  Pressing the
reset button on the arduino restarts the sketch and we see the stored boot
number incrementing to 1 and 2, etc.

Holding the test button down while pressing the arduino reset button causes
the code to invalidate the EEPROM data, starting the boot count at 0::

    Start sketch
    Initializing EEPROM
    Number of boots saved: 0
    Start sketch
    Number of boots saved: 1
    Start sketch
    Number of boots saved: 2
    ################################### test button held down here
    Start sketch
    Button pressed, EEPROM invalidated!
    Initializing EEPROM
    Number of boots saved: 0
    Start sketch
    Number of boots saved: 1

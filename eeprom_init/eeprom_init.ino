/////////////////////////////////////////////////////////////////////////
// Example code to show CRC initialization of EEPROM data.
//
// A simple Arduino board is used, with a SPST button connected from pin
// 10 to GND.  This button is used to force an EEPROM initialization for
// testing because after the first initialization we would not normally
// see the EEPROM initialize.
/////////////////////////////////////////////////////////////////////////

#include <EEPROM.h>

// pin definition for the switch
const int SwitchPin = 10;   // Iota = B6

// struct holding data saved to EEPROM
typedef struct
{
  int num_boots;            // all EEPROM data in here
} EepromData;

// place to save EEPROM data CRC
unsigned long Checksum = 0;

// in-memory copy of EEPROM data
EepromData Data;

// define start addresses in EEPROM of various fields
const int EepromAddrChecksum = 0;
const int EepromAddrData = EepromAddrChecksum + sizeof(Checksum);

//----------------------------------------
// Calculate the CRC of the data in memory
// This code from: https://www.arduino.cc/en/Tutorial/LibraryExamples/EEPROMCrc
//----------------------------------------

unsigned long eeprom_crc(void)
{
  const unsigned long crc_table[16] =
      {0x00000000, 0x1db71064, 0x3b6e20c8, 0x26d930ac,
       0x76dc4190, 0x6b6b51f4, 0x4db26158, 0x5005713c,
       0xedb88320, 0xf00f9344, 0xd6d6a3e8, 0xcb61b38c,
       0x9b64c2b0, 0x86d3d2d4, 0xa00ae278, 0xbdbdf21c
      };
  unsigned long crc = ~0L;

  for (unsigned long index = EepromAddrData; index < sizeof(Data); ++index)
  {
    crc = crc_table[(crc ^ EEPROM[index]) & 0x0f] ^ (crc >> 4);
    crc = crc_table[(crc ^ (EEPROM[index] >> 4)) & 0x0f] ^ (crc >> 4);
    crc = ~crc;
  }

  return crc;
}

//----------------------------------------
// Saves the in-memory data to EEPROM and refreshes the CRC.
//----------------------------------------

void save_eeprom_data(void)
{
  // save in-memory data to EEPROM
  EEPROM.put(EepromAddrData, Data);

  // calculate CRC of the data
  Checksum = eeprom_crc();
  EEPROM.put(EepromAddrChecksum, Checksum);
}

//----------------------------------------
// Gets the data in EEPROM and refreshes the RAM copies.
//
// If the EEPROM checksum is invalid, initializes EEPROM and sets
// in-memory copy to initialized values.
//----------------------------------------

void restore_eeprom_data(void)
{
  // get data checksun
  EEPROM.get(EepromAddrChecksum, Checksum);

  // get actual CRC of data and ensure same
  if (Checksum != eeprom_crc())
  {
    Serial.println("Initializing EEPROM");

    // initialize data
    Data.num_boots = 0;

    // and save initialized data (also updates EEPROM checksum)
    save_eeprom_data();
  }

  // get data stored in EEPROM
  EEPROM.get(EepromAddrChecksum, Checksum);
  EEPROM.get(EepromAddrData, Data);
}


void setup()
{
  // start the serial connection, announce start
  Serial.begin(9600);
  
#if defined(__AVR_Atmega32U4__)
  while (!Serial)       // needed if running AtMega32U4 leonardo/teensy 2/etc
    ;
#endif

  Serial.println("Start sketch");
    
  // the SwitchPin must be INPUT_PULLUP (switch active LOW)
  pinMode(SwitchPin, INPUT_PULLUP);

  // check the switch
  // if down, write 0 to EEPROM CRC value
  if (digitalRead(SwitchPin) == LOW)
  {
    Serial.println("Button pressed, EEPROM invalidated!");
    Checksum = 0;
    EEPROM.put(EepromAddrChecksum, Checksum);
  }
  
  // get data from EEPROM
  restore_eeprom_data();

  // show the number of boots so far
  Serial.print("Number of boots saved: ");
  Serial.println(Data.num_boots);

  // update the number of boots and save data to EEPROM
  Data.num_boots += 1;
  save_eeprom_data();
}


void loop()
{
}

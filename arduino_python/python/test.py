#!/usr/bin/env python3

"""
A program to test communicating with and controlling a sketch
on the Arduino.

Usage:  test.py [-h] [-i <deviceID>] [-v <vendorID>] [-p <productID>]

where -h           prints this help, and
      <deviceID>   is an optional ID string shown by the device
      <vendorID>   is an optional product ID value
      <productID>  is an optional product ID

When running, type commands which will be passed to the sketch
running on the DigiThing.

A DigiThing device should respond to the "ID;" command with a
string starting with the value specified by <deviceID>.
"""

import os
import sys
import time
import datetime
import getopt
import usb
import traceback

try:
    import log
    log = log.Log('test.log', log.Log.DEBUG)
except AttributeError:
    # means log already set up
    pass
except ImportError as e:
    # if we don't have log.py, don't crash
    # fake all log(), log.debug(), ... calls
    def logit(*args, **kwargs):
        pass
    log = logit
    log.debug = logit
    log.info = logit
    log.warn = logit
    log.error = logit
    log.critical = logit
import digithing


def str2int(s):
    """Convert string 's' to an integer.

    String could start with 0X meaning it's hex digits.
    """

    s = s.lower()
    if s.startswith('0x'):
        return int(s, 16)
    return int(s)

def control_digitest(digitest):
    """Send user commands to the sketch and receive replies.

    digitest  the DigiTest device to use
    """

    while True:
        try:
            cmd = input('test> ')
        except (EOFError, KeyboardInterrupt):
            print()
            return

        if not cmd:
            continue        # ignore blank commands

        if cmd.startswith('quit'):
            return          # 'quit' terminates the program

        log(f"Sending command '{cmd}' to the DigiTest device.")
        result = digitest.do_cmd(cmd)
        log(f"Result was: {result}")
        print(result)

def find_required_devices(device_id, vendor_id, product_id):
    """Find and return the required devices.

    device_id   the ID of the device to use
    vendor_id   the vendor ID number
    product_id  the product ID
    """

    log(f"find_required_devices: device_id='{device_id}', vendor_id={vendor_id}, product_id={product_id}")

    print('Looking for DigiTest device ... ', end='')
    sys.stdout.flush()
    digitest = None
    try:
        digitest = digithing.DigiThing(device_id, vendor_id=vendor_id,
                                       product_id=product_id)
    except digithing.DigiThingError:
        print('No DigiTest devices found')
    
    if digitest is None:
        raise Exception("Can't find a DigiTest device!?")

    return digitest

def usage(msg=None):
    """Print the module docstring plus the optional message."""

    if msg:
        print('-' * 60)
        print(msg)
        print('-' * 60, '\n')
    print(__doc__)

def excepthook(type, value, tback):
    """Capture any unhandled exceptions."""

    msg = '\n' + '=' * 80
    msg += '\nUncaught exception:\n'
    msg += ''.join(traceback.format_exception(type, value, tback))
    msg += '=' * 80 + '\n'
    log(msg)
    print(msg)

# plug our handler into the python system
sys.excepthook = excepthook

# check the command line args
try:
    opts, args = getopt.gnu_getopt(sys.argv, "hi:p:v:",
                                   ["help", "id=", "product=", "vendor="])
except getopt.GetoptError:
    usage('Bad option')
    sys.exit(10)

digitest_id = None
product_id = None
vendor_id = None

for opt, arg in opts:
    if opt in ("-h", "--help"):
        usage()
        sys.exit(0)
    elif opt in ("-i", "--id"):
        digitest_id = arg
    elif opt in ("-p", "--product"):
        product_id = arg
    elif opt in ("-v", "--vendor"):
        vendor_id = arg

if len(args) != 1:
    usage()
    sys.exit(10)

# convert the vendor_id and product_id strings to integer values
if product_id:
    product_id = str2int(product_id)
if vendor_id:
    vendor_id = str2int(vendor_id)

# check we have the required devices online
digitest = find_required_devices(digitest_id, vendor_id, product_id)

# start the measurements, save data to given file
print("""
-----------Interactive Commands-----------------
H;       send help text to console
ID;      get device identifier string
T;       get the time since boot
------------------------------------------------
""")
print("Type 'quit' to quit.")
print()

control_digitest(digitest)

#!/usr/bin/env python3

"""
A class to interface to a Digi* device such as a DigiTest, etc.
"""

import os
import sys
import time
import serial
import usb
from serial.tools.list_ports import comports
try:
    import log
    log = log.Log('digithing.log', log.Log.DEBUG)
except AttributeError:
    # means log already set up
    pass
except ImportError as e:
    # if we don't have log.py, don't crash
    # fake all log(), log.debug(), ... calls
    def logit(*args, **kwargs):
        pass
    log = logit
    log.debug = logit
    log.info = logit
    log.warn = logit
    log.error = logit
    log.critical = logit



###############################################################################
# A class to interface with a DigiThing device.
#
#   # create object, optional baudrate, ignore named devices
#   dv = DigiThing(description, ignore=None, baudrate=115200)
#   # execute command, return results
#   response = dv.do_cmd('ID;')
#   # close device
#   dv.close()
###############################################################################

class DigiThingError(Exception):
    """Exception raised if no DigiThing found or other error."""

    pass

class DigiThing:

    # timeout on serial port in seconds
    SerialTimeout = 0.10


    def __init__(self, description=None, ignore=None, baudrate=115200,
                       vendor_id=None, product_id=None):
        """Connect to a single DigiThing device.

        description  optional identifying string of the DigiThing to be used
        ignore       list of devices to ignore
        baudrate     speed of serial communication
        vendor_id    optional vendor ID string
        product_id   optional product ID string
        """

        log.debug(f"description={description}, ignore={ignore}, baudrate={baudrate}")
        log.debug(f"vendor_id={vendor_id}, product_id={product_id}")

        # prepare internal state
        self.ser = None
        self.baudrate = baudrate
        self.description = description
        self.vendor_id = vendor_id
        self.product_id = product_id
        self.ignore = []
        if ignore:
            self.ignore = ignore
            log.debug(f'.ignore={self.ignore}')

        # get *all* DigiThing devices on the USB bus
        # note that if the user doesn't supply vendor_id and product_id then
        # we test all USB devices found (slow!)
        result = []
        for usb_dev in serial.tools.list_ports.comports():
            log(f"scanning '{usb_dev}': usb_dev.vid={usb_dev.vid}, usb_dev.pid={usb_dev.pid}")
            if self.vendor_id and self.product_id:
                log(f"Checking device usb_dev.vid={usb_dev.vid}, usb_dev.pid={usb_dev.pid}")
                if (usb_dev.vid == self.vendor_id
                        and usb_dev.pid == self.product_id):
                    log(f'Found device with {self.vendor_id}:{self.product_id}')
                    if usb_dev.device not in self.ignore:
                        log(f'Appending device usb_dev.vid={usb_dev.vid}, usb_dev.pid={usb_dev.pid}')
                        result.append(usb_dev)
            else:
                log(f"Appending device usb_dev.vid={usb_dev.vid}, usb_dev.pid={usb_dev.pid}")
                result.append(usb_dev)

        log.debug(f'found devices:\n{result}')

        if len(result) < 1:
            raise DigiThingError('No DigiThing devices found.')

        # keep only the devices that identify as the desired "description"
        # don't do this if 'description' is None
        if self.description:
            things = []
            for usb in result:
                log.debug(f"Checking USB device '{usb}'")
                # open a serial connection to the possible DigiThing
                self.ser = serial.Serial(port=usb.device, baudrate=baudrate)
                self.ser.timeout = DigiThing.SerialTimeout
                log('Point 1')

                # check we actually have a real DigiThing
                self._flush_input()                         # flush any pending output
                log('Point 2')
                id_string = self.do_cmd('id;')              # ask for an ID string
                log('Point 3')
                log.debug(f"id_string='{id_string}'")
                if id_string.startswith(self.description):
                    things.append(usb)
                self.ser.close()
                self.ser = None
            result = things

        if len(result) < 1:
            raise DigiThingError(f'No DigiThing devices with description={self.description} found.')

        log.debug(f'found devices:\n{result}')

        # make sure we have only one
        if len(result) > 1:
            raise DigiThingError(f'More than one DigiThing device with description={self.description} found.')

        self.device = result[0]                     # save possible DigiThing

        # open a serial connection to the possible DigiThing
        self.port = self.device.device
        self.ser = serial.Serial(port=self.port, baudrate=baudrate)
        self.ser.timeout = 0.05                     # nice short timeout

        log.debug(f"Using DigiThing({self.description}) on port '{self.port}'")

    def _readchar(self):
        """Read one character from the serial device.

        Retry on SerialException.
        Returns an empty string if no character read.
        """

        for _ in range(10):
            try:
                return str(bytes(self.ser.read()), encoding='utf-8')
            except (serial.serialutil.SerialException, OSError):
                log.debug(f'Retrying because of exception.')
                time.sleep(0.5)

        raise DigiThingError('Digivolt device disconnected') from None

    def _read_line(self):
        """Read one line from the 'ser' connection."""
    
        line = ''
        count = 0
        while count < 3:
            char = self._readchar()
            if char:
                if char == '\n':
                    log.debug(f"_read_line: returning '{line}'")
                    return line
                elif char != '\r':
                    line += char
                    count = 0
            else:
                count += 1
    
        return ''

    def _flush_input(self):
        """Throwaway all input from the device until a timeout."""

        count = 0
        while count < 3:
            line = self._read_line()
            if line:
                count = 0
            else:
                count += 1

    def do_cmd(self, cmd):
        """Send command to DigiThing, return response."""

        log.debug(f"do_cmd: cmd='{cmd}'")

        if cmd[-1] != '\n':
            cmd += '\n'
        try:
            log('Point 1.1')
            self.ser.write(bytes(cmd, encoding='utf-8'))
            log('Point 1.2')
        except (serial.serialutil.SerialException, OSError):
            raise DigiThingError('Digivolt device disconnected') from None

        # read response
        response = []
        count = 0
        while count < 2:
            log('Point 1.3')
            line = self._read_line()
            log('Point 1.4')
            if line:
                line = line.strip()
                log.debug(f"do_cmd: line='{line}'")
                response.append(line)
                count = 0
            else:
                count += 1

        response = '\n'.join(response)
        log.debug(f"do_cmd: return response='{response}'")
        return response

    def close(self):
        """Close the DigiThing."""

        if self.ser:
            self.ser.close()

    def __del__(self):
        """Delete the DigiThing device."""

        self.close()

    def __str__(self):
        return f'{self.description} (baudrate={self.baudrate})'

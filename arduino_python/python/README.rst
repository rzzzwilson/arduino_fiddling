The **test.py** file holds the code to communicate with and control
the arduino code.

You execute **test**.py with enough parameters for the code to identify
the correct USB device.  The parameters are::

    <deviceID>  This is a string returned by a device when given the "ID;" command
    <vendorID>  The vendor number of the USB device
    <productID> The product number of the USB device

When specifying parameters you will find a DigiThing device if you supply any 
of these combinations of parameters::

    <deviceID>
    <vendorID> <productID>
    <deviceID> <vendorID> <productID>

So, simple examples of finding a DigiThing sketch running on an Arduino UNO is::

    python3 test.py -i DigiTest
    python3 test.py -v 0x2341 -p 0x0043
    python3 test.py -i DigiTest -v 0x2341 -p 0x0043

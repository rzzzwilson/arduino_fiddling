pyParsel
========

pyParsel is a system that allows communications between a microcontroller
programmed by the Arduino system and a python program running on a computer.
Communication is via the USB cable.

Other systems like pyFirmata (https://pypi.org/project/pyFirmata/) seem
more heavyweight.  Plus pyFirmata doesn't help with discovery of devices,
something pyParsel tries to automate.

The software here is in two parts.  The firmware on the microcontroller
handles commands from the computer and sends replies back.  On the computer
the pyParsel class encapsulates all communication with the microcontroller.

Device discovery
----------------

Device discovery is designed to automate, as much as possible, connecting the
computer to multiple pyParsel microcontrollers.  The scenario is that there may
be many devices controlled by pyParsel microcontrollers and we want an automatic
process to discover and use those microcontrollers.

One particular use case is four microcontrollers managing devices to set the
input voltage input to a circuit and monitor the voltage at three points in that
circuit.  We would like to write some "management" python code that would prompt
the connection of various devices and the discovery of the correct device to be
controlled/monitored via pyParsel.

One aid to discovery is for each unique pyParsel device to have a unique
*usb_product* value.  This is done by creating an entry in the Arduino
*boards.txt* for each pyParsel device.  This *usb_product* value can be found
from the USB device and speeds up finding the appropriate device.

Another way to identify a pyParsel device is for the device to respond to an
"ID;" command with a unique string that identifies the device type.  If the 
device doesn't respond to the "ID;" command it isn't a pyParsel device.  This
approach is necessarily slower than the "boards.txt" approach above.

Even with all that identification, pyParsel code can't distinguish between two
devices that measure voltage, so there will need to be some method of specifying
which devices have already been recognized and they can be ignored in a rescan
of the USB bus looking for recently added devices.



Microcontroller
---------------

The firmware 


This little project shows how to have python running on a desktop
computer (or Raspberry Pi, etc) and controlling an Arduino device.

Just compile the sketch on to an arduino.

Once the sketch is running execute the *test.py* program.  Make sure you
quit the arduino IDE so the USB port is free before running *test.py*.
Here's what using it looks like::

    $ python3 test.py
    Looking for DigiTest device ... found:
    
    -----------Interactive Commands-----------------
    H;       send help text to console
    ID;      get device identifier string
    T;       get the time since boot
    ------------------------------------------------
    
    Type 'quit' to quit.
    
    test> id;
    DigiTest 1.0
    test> h;
    -----------Interactive Commands-----------------
    H;       send help text to console
    ID;      get device identifier string
    T;       get the time since boot
    ------------------------------------------------
    test> t;
    12.86s since boot.
    test> t;
    14.66s since boot.
    test> quit
    $

/*
 * Firmware for the DigiTest device.
 * https://gitlab.com/rzzzwilson/arduino_fiddling/-/tree/master/arduino_python
 *
 * Must modify /Applications/Arduino.app/Contents/Java/hardware/arduino/avr/boards.txt
 * to include this line "leonardo.build.usb_product="DigiTest" before uploading.  This
 * is so the enumeration of USB devices will see "DigiTest" as 
 */

// program name
#define PROGRAM_NAME  "DigiTest"

// version of the software
#define VERSION       "1.1"

//-----
// Buffer for external command gathering
//-----

// buffer, etc, to gather external command strings
#define MAX_COMMAND_LEN   16
#define COMMAND_END_CHAR    ';'
char CommandBuffer[MAX_COMMAND_LEN + 1];
int CommandIndex = 0;

// messages for command code
const char *ErrorMsg = "ERROR";
const char *OKMsg = "OK";

//-----
// Miscellaneous
//-----

// LED pin and timing
const int LEDPin = LED_BUILTIN;
const int BlinkPeriod = 100;  // milliseconds

// macro to turn off warnings about "unused parameter" in functions
#define UNUSED(v) (void) (v)

// stuff for the buffer for log messages
const int LogBuffSize = 1000;   // most of the remaining RAM
char *log_buffer;
char *log_ptr;

//##############################################################################
// Utility routines.
//##############################################################################

//----------------------------------------
// Return the number of free bytes of RAM.
//----------------------------------------

unsigned int free_ram(void)
{
    extern unsigned int __heap_start;
    extern unsigned int *__brkval;
    unsigned int v;     // measuer free RAM from this variable on stack

    return (unsigned int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval);
}


//----------------------------------------
// Dump the log buffer to serial
//----------------------------------------

void dump_mem(void *addr, int len)
{
  Serial.print((long unsigned int) addr, HEX);
  Serial.print(": ");

  char *ptr = (char *) addr;
  
  for (int i = 0; i < len; ++i)
  {
    if (*ptr == 0)
      break;

    Serial.print(" ");
    Serial.print(*ptr);
    ++ptr;
  }
  
  Serial.println("");
}

//----------------------------------------
// Save a message in the log buffer
//----------------------------------------

void my_log(const char *msg, bool eol)
{
  static bool new_line = true;

  if (strlen(log_buffer) + strlen(msg) > LogBuffSize - 20)
  {
    Serial.println("Log buffer overflow");
    return;
  }

  if (new_line)
  {
    char prefix[20];

    sprintf(prefix, "%lu:", millis());
    strcpy(log_ptr, prefix);
    log_ptr += strlen(prefix);
    
    new_line = false;
  }
  
  strcpy(log_ptr, msg);
  log_ptr += strlen(msg);
  
  if (eol)
  {
    strcpy(log_ptr, "\n");
    ++log_ptr;
    new_line = true;
  } 
  *log_ptr = '\0';
}

//----------------------------------------
// Write log buffer to serial & reinitialize
//----------------------------------------

void dump_log(void)
{
  Serial.println(log_buffer);
  log_ptr = log_buffer;
  *log_ptr = '\0';
}

//----------------------------------------
// Convert a string to uppercase, in situ.
//----------------------------------------

void str2upper(char *str)
{
  while (*str)
  {
    *str = toupper(*str);
    ++str;
  }
}

//##############################################################################
// Code for the "external" commands from the serial port.
//##############################################################################

//----------------------------------------
// Get help:
//     H;
//----------------------------------------

void xcmd_help(char *cmd)
{
  UNUSED(cmd);

  Serial.println(F("-----------Interactive Commands-----------------"));
  Serial.println(F("H;       send help text to console"));
  Serial.println(F("ID;      get device identifier string"));
  Serial.println(F("T;       get the time since boot"));
  Serial.println(F("D;       dump internal log buffer"));
  Serial.println(F("F;       get free memory in bytes"));
  Serial.println(F("------------------------------------------------"));
}

//----------------------------------------
// Get the identifier string:
//     ID
//----------------------------------------

void xcmd_id(char *cmd)
{
  // if not legal, complain
  if (strcmp(cmd, "ID"))
  {
    Serial.println(ErrorMsg);
    return;
  }

  // generate ID string and return
  Serial.print(F(PROGRAM_NAME));
  Serial.print(F(" "));
  Serial.println(F(VERSION));
}

//----------------------------------------
// Get the time since boot in milliseconds:
//     T
//----------------------------------------

void xcmd_time(char *cmd)
{
  // if not legal, complain
  if (strcmp(cmd, "T"))
  {
    Serial.println(ErrorMsg);
    return;
  }

  Serial.print((float) millis() / 1000);
  Serial.println("s since boot.");
}

//----------------------------------------
// Dump the log buffer to Serial:
//     D
//----------------------------------------

void xcmd_dump(char *cmd)
{
  // if not legal, complain
  if (strcmp(cmd, "D"))
  {
    Serial.println(ErrorMsg);
    return;
  }

  dump_log();
}

//----------------------------------------
// Display free memory on Serial:
//     F
//----------------------------------------

void xcmd_free(char *cmd)
{
  // if not legal, complain
  if (strcmp(cmd, "F"))
  {
    Serial.println(ErrorMsg);
    return;
  }

//  unsigned long result = free_ram();
//  Serial.print("free: ");
  Serial.println(free_ram());
}

//----------------------------------------
// Process an external command.
//     cmd     address of command string buffer
//     index   index of last char in string buffer
// 'cmd' is '\0' terminated.
//----------------------------------------
void do_extern_cmd(char *cmd)
{
  // ensure everything is uppercase
  str2upper(cmd);

  // process the command
  switch (cmd[0])
  {
    case 'H':
      xcmd_help(cmd);
      return;
    case 'I':
      xcmd_id(cmd);
      return;
    case 'T':
      xcmd_time(cmd);
      return;
    case 'D':
      xcmd_dump(cmd);
      return;
    case 'F':
      xcmd_free(cmd);
      return;
  }

  Serial.println(ErrorMsg);
}

void do_external_commands(void)
{
  bool overflow = false;    // "true" if command buffer overflowed

  // gather any commands from the external controller
  while (Serial.available())
  {
    char ch = Serial.read();

    // ignore newlines
    if (ch == '\n')
      continue;

    if (CommandIndex < MAX_COMMAND_LEN)
    {
      // only add if command not over-length
      CommandBuffer[CommandIndex++] = ch;
    }
    else
      overflow = true;

    if (ch == COMMAND_END_CHAR)   // if end of command, execute it
    {
      if (overflow)
      {
        overflow = false;
        Serial.println(F("TOO LONG"));
      }
      else
      {
        my_log("Doing cmd: ", false);
        my_log(CommandBuffer, true);
        CommandBuffer[CommandIndex-1] = '\0'; // remove final ';'
        do_extern_cmd(CommandBuffer);
        CommandIndex = 0;
      }
    }
  }
}

//##############################################################################
// Standard Arduino setup()function.
//##############################################################################

void setup(void)
{
  // initialize the serial console
  Serial.begin(115200);

#ifdef BOOT_DELAY
  delay(4000);
#endif
  
#ifdef ANNOUNCE
  // announce the start
  Serial.print(PROGRAM_NAME);
  Serial.print(" ");
  Serial.print(VERSION);
  Serial.println(" - ready!");
#endif

  // initialize logging buffer
  log_buffer = (char *) malloc(LogBuffSize);
  *log_buffer = '\0';
  log_ptr = log_buffer;
  my_log((char *) "Log initialized", true);

  // prepare the LED
  pinMode(LEDPin, OUTPUT);
  digitalWrite(LEDPin, 0);
}

//##############################################################################
// Standard Arduino loop() function.
//##############################################################################

void loop(void)
{
  static unsigned long last_blink = 0;
  static bool state = false;        // true means LED is on
  
  // handle any commands from the serial port
  do_external_commands();

  // blink the LED
  if (millis() - last_blink > BlinkPeriod)
  {
    last_blink = millis();
    state = !state;
    if (state)
      digitalWrite(LEDPin, 1);
    else
      digitalWrite(LEDPin, 0);
  }
}

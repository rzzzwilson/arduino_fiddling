SoftSwitch
==========

A little bit of arduino code to latch the hardware softswitch circuit and
detect another button press thereby turning the power OFF.

This version runs on the Iota.

Note the Iota pin mappings used in the code:

    D2      SENSE        (pin 0 on arduino)
    D1      HOLD         (pin 2 on arduino)
    D6      LED_BUILTIN  (oin 12 on arduino)

SoftSwitch Limitations
----------------------

Input voltage range:    7 ~ 20v
Max input current:      1.0A

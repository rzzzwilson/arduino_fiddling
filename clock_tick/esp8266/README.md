This shows how to make a ticking clock on an ESP8266 12-F module.
Actiaually using a Wemos D1 Mini as testbed, but same same.

Connections
-----------

![schematic](ESP8266_Schematic.png "schematic")

Preparing sound data
--------------------

The file *TickTockSample.wav* is the starting point.  That file is a sample
of one "tick" and one "tock" in a file that is 16-bit stereo sampled at
44100 Hz.  The final target is two files: *tick.raw* and *tock.raw*.
Those files will be 16-bit mono and sampled at 32 kHz.  This is how to
get that from *TickTockSample.wav*.

* Convert the stereo file *TickTockSample.wav* to a mono file *test.wav*:

    ffmpeg -i TickTockSample.wav -ac 1 test.wav

* Use Audacity to create two files *tick.wav* and *tock.wav* containing
  just the tick and tock sounds.  These are 16 bit, mono 44100 Hz files.

* OPTIONAL.  Amplify the *\*.wav* files using Audacity.

* Using the Unix *xxd* command, convert the *tick.wav* and *tock.wav*
  files to *tick.xxd* and *tock.xxd*:

    xxd -i tick.wav  > tick.xxd
    xxd -i tock.wav  > tock.xxd

* Edit the *\*.xxd* files to add the "const" and "PROGMEM" words into the 
  first line:

    const unsigned char tick_mp3[] PROGMEM = {

  Also remove the last line defining a "length" name.

* Then put the contents of the *\*.xxd* files into the *tick_tock.h* file.


Summary
-------

Well, the schematic and code do work on the ESP8266, but the souds are too soft
to be usable.  We could probably get good results with an amplifier but that
is too complicated for ading to the clock series.  So this is a dead-end.

    

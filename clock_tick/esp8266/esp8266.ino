#include "AudioFileSourcePROGMEM.h"
#include "AudioGeneratorWAV.h"
#include "AudioOutputI2SNoDAC.h"

#include "tick_tock.h"

AudioGeneratorWAV *wav;
AudioFileSourcePROGMEM *file;
AudioOutputI2SNoDAC *out;

bool state = false;


void setup()
{
  Serial.begin(115200);
  delay(2500);
  Serial.printf("\nWAV start\n");

  audioLogger = &Serial;
  file = new AudioFileSourcePROGMEM(tick_wav, sizeof(tick_wav));
  out = new AudioOutputI2SNoDAC();
  wav = new AudioGeneratorWAV();
  wav->begin(file, out);
}

void loop()
{
  if (wav->isRunning())
  {
    if (!wav->loop())
    {
      wav->stop();
      state = !state;
    }
  }
  else
  {
    Serial.printf("WAV done\n");
    delay(900);
    if (state)
      file = new AudioFileSourcePROGMEM(tock_wav, sizeof(tock_wav));
    else
      file = new AudioFileSourcePROGMEM(tick_wav, sizeof(tick_wav));
    wav->begin(file, out);
  }
}

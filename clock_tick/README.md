The code and data here is an experiment to see if we can get 
reasonable sound from an Uno with PCM.  This is a possible
enhancement (?) for the CryptiClock and SmartClock projects.

There is also test code for an ESP8266.


Creating sound data
-------------------

Download a sound file from youtube.com.  The file downloaded is 
*ClockTick.m4a*.

Use Audacity to select out one "tick" and "tock".  Convert to mono.
Store that in *tick_tock.wav*.  That file is Microsoft PCM, 8 bit,
mono 44100 Hz data, with header.

Split the combined tick+tock into two files, one containing the "tick"
and the other is the "tock", files are *tick.wav* and *tock.wav*.

Use ffmpeg to convert the files to a sample rate of 8000Hz:

    ffmpeg -i tick.wav -ar 8000 tick2.wav; mv tick2.wav tick.wav
    ffmpeg -i tock.wav -ar 8000 tock2.wav; mv tock2.wav tock.wav

Using Audacity convert the two files into "raw" data, ie, a headerless
file.  That produces *tick.raw* and *tock.raw*.

There are multiple ways to get hex data from the "raw" files.  I used
my *fdump* utility to create a HEX+ASCII dump of each file and edited
the file into a C++ data form.  The resulting files are *tick.hex*
and *tock.hex*.

The *\*.py* files are used to multiply the data values in a dataset
by a set scale value.  Do this to get maximum usable volume.  The 
data arrays in the *\*.ino* files are adjusted, the *\*.hex* files
are the original values, unadjusted.

ESP8266
-------

To move this to the ESP8266:

https://github.com/earlephilhower/ESP8266Audio

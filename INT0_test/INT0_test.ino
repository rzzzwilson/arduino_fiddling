//Interrupts using Arduino

#define INTPIN  2

volatile unsigned int count = 0;                      

void setup()                                                      
{
  Serial.begin(115200);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(INTPIN, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(INTPIN), buttonPressed1, FALLING);
}

void loop()                                                      
{  
  Serial.println(count);
  delay(100);   
}

void buttonPressed1(void)
{
  // pin is LOW, wait a bit and check again
  for (int i = 0; i < 50; ++i)
  {
    delay(1);
    if (digitalRead(INTPIN))
      return;
  }
  
  count += 1;
}

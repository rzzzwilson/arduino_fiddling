# OutageLogger

A small bit of hardware and firmware to log power outages to a microSD
card.

## Hardware

The proof of concept hardware is:

* An Arduino Uno
* A microSD card breakout board, SPI interface
* A DS1307 RTC board, I2C interface

That's all that is needed.

### Connections

The firmware assumes the following connections.

For the RTC:

| Arduino |  RTC     |
|---      |---       |
|  5v     |  Vcc     |
|  GND    |  GND     |
|  A4     |  SDA     |
|  A5     |  SCL     |

For the microSD board:

| Arduino |  microSD |
|---      |---       |
|  5v     |  Vcc     |
|  GND    |  GND     |
|  10     |  CS      |
|  11     |  SI      |
|  12     |  SO      |
|  13     |  SCK     |

# Data Format

After operation for a while there might be one or more files with names like
"YYYY_MM.DAT" where the YYYY is the year of the outage and MM is the month number.

Data in these files will have lines of this form::

    0220209T201100,20220209T213558

Where the dates are in ISO8601 format::

    YYYYMMDDTHHMMSS

The first date in each line is the start of the outage to the previous minute.
The second date in each line is the time the outage ended.

//-------------------------------------------------------------------------------
// Code to log power outages to a microSD card.
//
// The microSD card should be formatted as FAT or FAT32, and be empty.
// On boot, if there is a timestamp file on the SD card the code assumes that
// a power outage has occurred and writes a "start,stop" line of times to a 
// logfile with the filename "YYYY_MM.DAT".  After boot, the code will write
// a timestamp to a file every minute.
//-------------------------------------------------------------------------------

#include <Wire.h>
#include "RTClib.h"
#include <SPI.h>
#include <SD.h>


RTC_DS1307 rtc;       // for the DS1307 RTC
File FileHandle;      // handle for file operations

// name of the timestamp file, must be 8.3
#define TIMESTAMP_FILENAME  "DT_STAMP.DAT"

// size of buffer used to assemble the date+time stamp string
// ISO8601 only needs 16 bytes: "YYYYMMDDTHHMMSS"
#define DTSTAMPLEN    16

// if DEBUG is defined messages are written to the serial console
#define DEBUG

// pin for the status LED
#define STATUS_LED    LED_BUILTIN

// number of flashes for an abend condition
enum AbendStatus
{
  ABEND_NOSD = 1,         // no microSD card found
  ABEND_NORTC = 2,        // no RTC found
  ABEND_SDINIT = 3,       // error initializing microSD card
  ABEND_TSREAD = 4,       // error reading timestamp file
  ABEND_TSWRITE = 5,      // error writing timestamp file
};


//--------------------------------------------------------
// Routine to flash the LED a number of times.
//     num  number of times to flash the LED
//
// Entire routine delays for 5 seconds.
//--------------------------------------------------------

void flash_led(int num)
{
  // delay to make entire function take 5 seconds
  int delay_count = 5000 - num*300;
  
  while (num--)
  {
    digitalWrite(STATUS_LED, HIGH);
    delay(150);
    digitalWrite(STATUS_LED, LOW);
    delay(150);    
  }
  
  delay(delay_count);
}

//--------------------------------------------------------
// A dead-end ABORT function.
//     msg  a string from the F() macro
//
// Only prints if DEBUG is defined.
//--------------------------------------------------------

void abend(const __FlashStringHelper *msg, enum AbendStatus status_num)
{
#ifdef DEBUG
  Serial.println();
  Serial.println(msg);
#endif

  // wait here forever
  while (1)
  {
    flash_led(status_num);
  }
}

//--------------------------------------------------------
// Provide a "printf()"-like function, shortens code.
//     msg  format string
//     ...  varargs stuff
//
// Only prints if DEBUG is defined.
// Problems will occur if the full message exceeds "tmp_buff" size.
//--------------------------------------------------------

void debugf(const char *fmt, ...)
{
#ifdef DEBUG
  va_list ptr;
  char tmp_buff[128];

  memset(tmp_buff, 0, sizeof(tmp_buff));

  va_start(ptr, fmt);
  vsprintf(tmp_buff, fmt, ptr);
  if (strlen(tmp_buff) > sizeof(tmp_buff))
  {
    Serial.println(F("Buffer overflow in debugf()!?"));
  }
  va_end(ptr);

  Serial.print(tmp_buff);
#endif
}

//--------------------------------------------------------
// Write a new timestamp string to the timestamp file.
//     now  is a DateTime object holding the current time
//--------------------------------------------------------

void update_timestamp(DateTime now)
{
  char ts_buffer[DTSTAMPLEN];

  // create the new timestamp (ISO8601)
  sprintf(ts_buffer, "%04d%02d%02dT%02d%02d%02d",
                     now.year(), now.month(), now.day(),
                     now.hour(), now.minute(), now.second());
                     
  // since we apparently can't rewind a file to overwrite it, delete the timestamp file
  if (SD.exists(TIMESTAMP_FILENAME))
  {
    SD.remove(TIMESTAMP_FILENAME);
    debugf("Removed timestamp file: %s\n", TIMESTAMP_FILENAME);
  }

  // write the timestamp string to the timestamp file
  FileHandle = SD.open(TIMESTAMP_FILENAME, FILE_WRITE);
  if (FileHandle)
  {
    debugf("Writing to timestamp file ... ");
    FileHandle.print(ts_buffer);
    FileHandle.close();
    debugf("done\n");
    debugf("Wrote timestamp: '%s'\n", ts_buffer);

#ifdef DEBUG
    // show we wrote a timestamp
    digitalWrite(STATUS_LED, HIGH);
    delay(150);
    digitalWrite(STATUS_LED, LOW);
#endif
  }
  else
  {
    abend(F("Error writing to the timestamp file!?"), ABEND_TSWRITE);
  }
}

//--------------------------------------------------------
// Write outage data to the current log file.
// 
// The start of the outage is in the timestamp file.
// The end of the outage is the time NOW.
// The log filename is calculated as "YYYY_MM.DAT".
//--------------------------------------------------------

void log_outage(void)
{
  char ts_buffer[DTSTAMPLEN];    // buffer to get outage start timestamp string
  char now_buffer[DTSTAMPLEN];   // buffer to construct outage end timestamp string
  
  // read latest timestamp
  FileHandle = SD.open(TIMESTAMP_FILENAME, FILE_READ);
  if (FileHandle)
  {
    int num_chars = FileHandle.read(ts_buffer, DTSTAMPLEN);
    if (num_chars <= 0)
    {
      abend(F("Error reading timestamp file!?"), ABEND_TSREAD);
    }
    ts_buffer[num_chars] = '\0';
    debugf("FileHandle.read() returned %d bytes: '%s'\n", num_chars, ts_buffer);
  }
  
  // generate "now" timestamp in ISO8601
  DateTime now = rtc.now();
  
  sprintf(now_buffer, "%04d%02d%02dT%02d%02d%02d",
                      now.year(), now.month(), now.day(),
                      now.hour(), now.minute(), now.second());
  
  // get filename to log to, year+month
  char log_file[12];          // make name of log file here, 8+3+1
  
  sprintf(log_file, "%04d_%02d.DAT", now.year(), now.month());
  debugf("log_file='%s'\n", log_file);
  
  // then append outage start/end dates to the end of the log file
  FileHandle = SD.open(log_file, FILE_WRITE);
  FileHandle.print(ts_buffer);
  FileHandle.print(",");
  FileHandle.println(now_buffer);
  FileHandle.close();

  debugf("log_outage: wrote '%s,%s'\n", ts_buffer, now_buffer);
}

//--------------------------------------------------------
// Prepare the device.
// 
// Start RTC and microSD card reader.
// Check if we need to log an outage.
//--------------------------------------------------------

void setup(void)
{
  // define status LED pin as output, ensure off
  pinMode(STATUS_LED, OUTPUT);
  digitalWrite(STATUS_LED, LOW);

  // init serial output
  Serial.begin(115200);
  delay(3000);
  debugf("Rebooted! -------------------------------------\n");

  // look for and prepare the RTC
  if (!rtc.begin())
  {
    abend(F("Couldn't find RTC"), ABEND_NORTC);
  }
  
  if (!rtc.isrunning())
  {
    Serial.println(F("RTC is NOT running!"));
    
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  }

  // look for and prepare the microSD card reader
  debugf("Initializing SD card ... ");
  if (!SD.begin(10))
  {
    abend(F("SD card initialization FAILED!"), ABEND_SDINIT);
  }
  debugf("done\n");

  // check if there is a timestamp file
  if (SD.exists(TIMESTAMP_FILENAME))
  {
    // timestamp exists, log an outage
    debugf("Timestamp file exists, so log an outage\n");
    log_outage();
  }
  else
  {
    debugf("There was no timestamp file\n");
  }

  debugf("End of setup()\n");
}

//--------------------------------------------------------
// Every minute, update timestam on the microSD card.
//--------------------------------------------------------

void loop(void)
{
  static int LastMinute = 0;    // minute last time we wrote a timestamp
  DateTime now = rtc.now();     // get current time (6 bytes allocated)
  
  if (LastMinute != now.minute())
  {
    // minute has changed, set new trigger minute
    LastMinute = now.minute();

    // update saved timestamp
    update_timestamp(now);
  }
}

// tone.ino
//
// Code to use a piezo speaker, in series with a 100ohm
// resistor between F0 and ground.
//

const int piezo_pin = 7;

void setup()
{
  DDRD = DDRD | _BV(7); // pin 7 as OUTPUT
  Serial.begin(115200);
  delay(2500);
  Serial.println("Ready");
}

int DELAY = 2000;
int step = 25;
unsigned long last_sec = 0;

void loop()
{
  unsigned long now = millis();
  unsigned long tick = now / 500;
  
  if (tick != last_sec)
  {
    last_sec = tick;
    DELAY -= step;
    if (DELAY < 50)
    {
      DELAY = 2000;
    }
    Serial.println(DELAY);
  }
  
  PORTD ^= _BV(7);
  delayMicroseconds(DELAY);
}

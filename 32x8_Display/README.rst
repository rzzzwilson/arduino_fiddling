Late in 2020 I received a 32x8 pixel display using the MAX7219 chip to drive
it.  This was instead of the expected order of 3 Arduino prototype shields!
So I have to exercise my free display!

The code here came from:

https://www.makerguides.com/max7219-led-dot-matrix-display-arduino-tutorial/

Directory *max* exercises the MD_Parola library.

Directory *max2* uses text sprites.
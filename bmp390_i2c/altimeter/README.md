Altimeter
=========

A simple little program that uses the BMP390 board to show:

* altitude
* local barometric pressure
* temperature

The local barometric pressure is currently hard-coded into the program.
Could either:

* make the ESP8266 scrape a page for the local airport to
  get the pressure, or
* add controls to adjust the pressure up/down

Display
-------

![Altimeter display](altimeter_display.png "Altimeter Display")

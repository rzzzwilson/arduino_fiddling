Test code to drive the BMP390 board using I2C.

There are two sub-directories that use the BMP390 board and
the 0.96" display:

    altimeter - a simple altimeter
    barometer - a simple barometer

Barometer
=========

A simple little program that uses the BMP390 board to show:

* barometeric pressure given a "base" altitude
* temperature

The base altitude is changed by pressing the up/down buttons.

Display
-------

![Barometer display](barometer_display.png "Barometer Display")

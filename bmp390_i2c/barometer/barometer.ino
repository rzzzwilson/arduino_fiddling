//
// Display barometric pressure on an 0.96" I2C display (SSD1306).
// The "base" altitude is adjusted with the up/down buttons.
//
// Runs on a Wemos D1 mini clone board.
//

#include <SPI.h>
#include <Wire.h>
#include <ezButton.h>
#include <EEPROM.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_Sensor.h>
#include "Adafruit_BMP3XX.h"


// define if debug is used
//#define DEBUG

#define VERSION_MAJOR   0
#define VERSION_MINOR   1

#define SCREEN_WIDTH   128    // OLED display width, in pixels
#define SCREEN_HEIGHT   32    // OLED display height, in pixels
#define TEXT_DISPLAY_Y  21    // Y coord of text display

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
// For D1 mini: D2 -> SDA, D1 -> SCL
#define OLED_RESET     -1   // Reset pin # (or -1 if sharing reset pin)
#define SCREEN_ADDRESS 0x3C // 0x3C for 128x32
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

Adafruit_BMP3XX bmp;

const float Meters2Feet = 3.28084;

#define PIN_UP    D6
#define PIN_DN    D7
ezButton btn_up(PIN_UP);
ezButton btn_dn(PIN_DN);

// timer stuff for display
unsigned long prev_update = 0;
const long update_interval = 1000;

// timer stuff for EEPROM save
// only save base altitude setting after 5 seconds of no change
bool save_pending = false;
unsigned long prev_save = 0;
const long save_interval = 5000;

// struct used to hold altimeter state
typedef struct
{
  int BaseAltitude;
} EepromData;

// default saved values
const int DefaultBaseAltitude = 999;  // feet

// the measured and calculated values
float Pressure = 1000.0;

// the EEPROM data CRC
unsigned long Checksum = 0;

// in-memory copy of EEPROM data, filled from EEPROM after reboot
EepromData Baro;

// define start addresses in EEPROM of various fields
const int EepromAddrChecksum = 0;
const int EepromAddrData = EepromAddrChecksum + sizeof(Checksum);


//********************************************************
// Code to get/set EEPROM values.
//--------------------------------------------------------
// Calculate the CRC of the data in memory
// This code from: https://www.arduino.cc/en/Tutorial/LibraryExamples/EEPROMCrc
//--------------------------------------------------------

unsigned long eeprom_crc(void)
{
  const unsigned long crc_table[16] =
  { 0x00000000, 0x1db71064, 0x3b6e20c8, 0x26d930ac,
    0x76dc4190, 0x6b6b51f4, 0x4db26158, 0x5005713c,
    0xedb88320, 0xf00f9344, 0xd6d6a3e8, 0xcb61b38c,
    0x9b64c2b0, 0x86d3d2d4, 0xa00ae278, 0xbdbdf21c
  };
  
  unsigned long crc = ~0L;

  for (unsigned long index = EepromAddrData; index < sizeof(Baro); ++index)
  {
    crc = crc_table[(crc ^ EEPROM.read(index)) & 0x0f] ^ (crc >> 4);
    crc = crc_table[(crc ^ (EEPROM.read(index) >> 4)) & 0x0f] ^ (crc >> 4);
    crc = ~crc;
  }

  return crc;
}

//--------------------------------------------------------
// Saves the in-memory data to EEPROM and refreshes the CRC.
//--------------------------------------------------------

void save_eeprom_data(void)
{
  EEPROM.put(EepromAddrData, Baro);

  Checksum = eeprom_crc();
  EEPROM.put(EepromAddrChecksum, Checksum);

  EEPROM.commit();
}

//--------------------------------------------------------
// Gets the data in EEPROM and refreshes the RAM copies.
//
// If the EEPROM checksum is invalid, sets EEPROM and
// in-memory copy to default values.
//--------------------------------------------------------

void restore_eeprom_data(void)
{
  // get expected data checksum
  EEPROM.get(EepromAddrChecksum, Checksum);

  // if checksum wrong, initialize EEPROM
  if (Checksum != eeprom_crc())
  {
    printf("Checksum error, initializing EEPROM\n");

    // force values to the default state
    Baro.BaseAltitude = DefaultBaseAltitude;
    save_eeprom_data();
  }

  // get data stored in EEPROM
  EEPROM.get(EepromAddrData, Baro);
}


void splash_screen()
{
  // show "BAROMETER" in a white rectangle at top, double size
  display.clearDisplay();
  display.fillRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT/2 + 1, SSD1306_WHITE);
  display.setTextColor(SSD1306_BLACK, SSD1306_WHITE);
  display.setTextSize(2);
  display.setCursor(11, 1);
  display.print("BAROMETER");

 
  // show scrolling version number at bottom
  display.setTextSize(1);
  for (int i = 0; i < 17; ++i)
  {
    display.setCursor(0, 17+i);
    display.setTextColor(SSD1306_WHITE);
    display.printf("     version %d.%d",
                   VERSION_MAJOR, VERSION_MINOR);
    display.display();
    
    delay(100);
    
    display.setCursor(0, 17+i);
    display.setTextColor(SSD1306_BLACK);
    display.printf("     version %d.%d",
                   VERSION_MAJOR, VERSION_MINOR);
  }
}

void update_display()
{
  // show "BAROMETER" in a white rectangle at top, double size
  display.clearDisplay();
  display.fillRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT/2 + 1, SSD1306_WHITE);
  display.setTextColor(SSD1306_BLACK, SSD1306_WHITE);
  display.setTextSize(2);
  display.setCursor(11, 1);
  display.print("BAROMETER");

  // show everything else on bottom line, white text on black background
  display.setTextColor(SSD1306_WHITE);
  display.setTextSize(1);
  display.setCursor(10, TEXT_DISPLAY_Y);
  display.printf("%6.2fhPa", Pressure);
  
  float temp = bmp.temperature;
  display.setCursor(90, TEXT_DISPLAY_Y);
  display.printf("%5.1fC", temp);

  // draw lines around outside
  display.drawLine(0, display.height()-1,
                   display.width()-1, display.height()-1,
                   SSD1306_WHITE);
  display.drawLine(0, 0,
                   0, display.height()-1,
                   SSD1306_WHITE);
  display.drawLine(display.width()-1, 0,
                   display.width()-1, display.height()-1,
                   SSD1306_WHITE);
  
  display.display();
}

//----------------------------------------------------
// Estimate the barometric pressure by adjusting pressure
// passed to bmp.readAltitude() until the returned altitude
// is the same as the base altitude.
//
// Returns the new pressure value.
//----------------------------------------------------

float calculate_pressure()
{
  return bmp.readPressure() / 100.0;

#ifdef JUNK
  // start with pressures above and below last value,
  float high_pressure = Pressure + 300.0;
  float low_pressure = Pressure - 300.0;
  float mid_pressure;
  int alt = 0;
  int delta = 2;
  
  while (true)
  {
    mid_pressure = (low_pressure + high_pressure) / 2;
    alt = bmp.readAltitude(mid_pressure);

    if (abs(alt - Baro.BaseAltitude) < delta)
    {
      break;
    }
    
    if (alt > Baro.BaseAltitude)
    {
      high_pressure = mid_pressure;
    }
    else if (alt < Baro.BaseAltitude)
    {
      low_pressure = mid_pressure;
    }
  }

  // here "mid_pressure" is the estimated barometric pressure
  return mid_pressure;
#endif
}

void setup()
{
  Serial.begin(115200);
#ifdef DEBUG
  delay(3500);            // ensure text will be displayed
  Serial.println();
#endif

  // configure buttons
  btn_up.setDebounceTime(50);
  btn_dn.setDebounceTime(50);

  EEPROM.begin(256);
  restore_eeprom_data();

  // see if we have the sensors connected
  bool failed = false;

  if (!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS))
  {
#ifndef DEBUG
    delay(3500);
#endif
    Serial.println("SSD1306 allocation failed");
    failed = true;
  }
  else
  {
    // Clear the display
    display.clearDisplay();
    display.display();
  }

  if (!bmp.begin_I2C())
  {
#ifndef DEBUG
    delay(3500);
#endif
    Serial.println("Could not find a valid BMP390 sensor, "
                   "check wiring!");
    failed = true;
  }

  // if any failure above wait here forever
  if (failed)
  {
    while (1)
    {
      yield();    // feed the watchdog!
      delay(250);
    }
  }

  // Set up BMP390 oversampling and filter initialization
  bmp.setTemperatureOversampling(BMP3_OVERSAMPLING_8X);
  bmp.setPressureOversampling(BMP3_OVERSAMPLING_4X);
  bmp.setIIRFilterCoeff(BMP3_IIR_FILTER_COEFF_3);
  bmp.setOutputDataRate(BMP3_ODR_50_HZ);

  // throw away first few measurements
  for (int i = 0; i < 3; ++i)
  {
    bmp.performReading();
    delay(30);
  }

#ifdef DEBUG
  Serial.printf("\n\nREADY\n");
#endif

  // show version numbers
  splash_screen();
}

void loop(void)
{
  unsigned long now = millis();

  // save EEPROM?
  if (save_pending && now - prev_save >= save_interval)
  {
    prev_save = now;
    save_pending = false;
    save_eeprom_data();
  }

  // update display?
  if (now - prev_update >= update_interval)
  {
    prev_update = now;
    Pressure = calculate_pressure();
    update_display();
  }

  // check the buttons
  btn_up.loop();
  if (btn_up.isPressed())
  {
    Baro.BaseAltitude += 5;
    prev_update = 0;    // force screen update
    prev_save = now;
    save_pending = true;
    prev_update  = 0;   // force display update
  }
  
  btn_dn.loop();
  if (btn_dn.isPressed())
  {
    Baro.BaseAltitude -= 5;
    prev_update = 0;
    prev_save = now;
    save_pending = true;
    prev_update  = 0;
  }
}

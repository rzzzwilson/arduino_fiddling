The sketch here tests a 3.3v SD card breakout board.

Connect the M0 Pro ISCP pins and pin 4 so:

SD            ICSP  M0           SD
vv         o vvvvvvvvvv          vv
             +--------+
DO     MISO -| o    o |- Vcc     3.3v
SCLK    SCK -| o    o |- MOSI    DI
x       RST -| o    o |- GND     GND
             +--------+

In addition, pin 4 on M0 goes to CS on the SD breakout.
This pin is configured in the sketch, allothers are assumed by
the SPI protocol.

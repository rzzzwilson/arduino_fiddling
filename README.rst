ArduinoFiddling
===============

A place for small test programs for Arduino and related microprocessors.

**schedule**

A small library to allow the arduino to handle asynchronous events
without using the *delay()* function.

**eeprom_init**

Code to handle EEPROM CRC checking and auto-initialization.

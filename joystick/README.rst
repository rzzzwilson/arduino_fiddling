Joystick
========

The projects here investigate various joystick projects.

joystick
--------

Simple joystick "smoke test".

tkinter
-------

A python tkinter program that reads joystick values and shows
the joystick 2-D position on a canvas.  C++ Arduino code plus
python.


#include <ezButton.h>

#define VRX_PIN  A0 // Arduino pin connected to VRX pin
#define VRY_PIN  A1 // Arduino pin connected to VRY pin
#define SW_PIN   2

int xValue = 0; // To store value of the X axis
int yValue = 0; // To store value of the Y axis
int swValue = 0;

ezButton button(SW_PIN);

void setup()
{
  Serial.begin(115200);

  pinMode(SW_PIN, INPUT_PULLUP);
  button.setDebounceTime(50);
}

void loop()
{
  xValue = analogRead(VRX_PIN);
  yValue = analogRead(VRY_PIN);
  
  button.loop();

  // print data to Serial Monitor on Arduino IDE
  Serial.print(xValue);
  Serial.print(",");
  Serial.print(yValue);
  Serial.print(",");
  if (button.getState())
    Serial.println("0");
  else
    Serial.println("1");

  delay(50);
}

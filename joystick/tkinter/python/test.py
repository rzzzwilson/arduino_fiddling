import sys
import serial
from digithing import DigiThing, DigiThingError

def wait_connection(prompt):
    """Wait until a PWKeep firmware device is connected."""

    # wait for a PWKeep device to be connected
    count = 10000
    while True:
        error = None
        try:
            return DigiThing()
        except DigiThingError as e:
            error = str(e)

        count += 1
        if count > 10000:
            if error:
                print('\r' + prompt + error + '       ', end='')
            count = 0

def main():
    # wait for a Uno to be connected
    prompt = 'Searching for an Uno board ... '
    print(prompt, end='')
    sys.stdout.flush()
    device = wait_connection(prompt)
    print('\r' + prompt + 'done' + ' '*50)

    while True:
        line = device.readline().strip()
        print(line)


try:
    main()
except KeyboardInterrupt:
    print()

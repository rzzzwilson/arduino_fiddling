test.py
=======

A python tkinter program to read data from the serial line and display
it on a 2-D canvas.

The data will be written by the C++ code running on the Uno.  The serial
message will be lines like:

    560,23,0

where the three comma-delimited fields will be:

* x-axis potentiometer reading (0..1023)
* y-axis potentiometer reading (0..1023)
* the button state, 0 means UP and 1 means DOWN

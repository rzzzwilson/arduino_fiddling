import sys
import time
import serial
import tkinter as tk
from digithing import DigiThing, DigiThingError

def wait_connection(prompt):
    """Wait until a PWKeep firmware device is connected."""

    # wait for a PWKeep device to be connected
    count = 10000
    while True:
        error = None
        try:
            return DigiThing()
        except DigiThingError as e:
            error = str(e)

        count += 1
        if count > 10000:
            if error:
                print('\r' + prompt + error + '       ', end='')
            count = 0

def move_dot():
    global dot_id
    global lastx, lasty

    data = device.readline().split(',')
    if len(data) == 3:
        (x, y, b) = data
        x = int(x)
        y = int(y)
        dx = x - lastx
        dy = y - lasty
    
        if b == '1':
            colour = colour_down
        else:
            colour = colour_up

        if dot_id:
            print(f"move(dot_id, {x=}, {y=})")
            canvas.move(dot_id, dx, dy)
        else:
            point_extent = ((x-radius, y-radius), (x+radius, y+radius))
            dot_id = canvas.create_oval(*point_extent, fill=colour)
        canvas.itemconfig(dot_id, fill=colour)
        (lastx, lasty) = (x, y)
    root.after(time_step, move_dot)


root = tk.Tk()
root.geometry('1024x1024')
root.title('Show joystick state')

canvas = tk.Canvas(root, width=1024, height=1024, bg='white')
canvas.pack(anchor=tk.CENTER, expand=True)

radius = 10
colour_up = 'blue'
colour_down = 'red'

x, y = 10, 10
lastx = lasty = 10
dot_id = None
time_step = 25

# wait for a Uno to be connected
prompt = 'Searching for an Uno board ... '
print(prompt, end='')
sys.stdout.flush()
device = wait_connection(prompt)
print('\r' + prompt + 'done' + ' '*50)

root.after(0, move_dot)

root.mainloop()
